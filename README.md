# feather
https://feathericons.com/

# Installation
`go get -u gitea.com/go-icon/feather`

# Usage
```go
icon := feather.Activity()

// Get the raw XML
xml := icon.XML()

// Get something suitable to pass directly to an html/template
html := icon.HTML()
```

# Build
`go generate generate.go`

# New Versions
To update the version of feather, simply change `featherVersion` in `feather_generate.go` and re-build.

# License
[MIT License](LICENSE)