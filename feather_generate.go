// +build ignore

package main

import (
	"archive/tar"
	"bytes"
	"compress/gzip"
	"flag"
	"fmt"
	"golang.org/x/net/html"
	"golang.org/x/net/html/atom"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"sort"
	"strings"
	"text/template"
)

var (
	oFlag string

	featherVersion = "4.24.1"
)

const (
	tgzName = "feathers.tgz"
	dirName = "feathers"
)

func init() {
	flag.StringVar(&oFlag, "o", "", "write output to `file` (default standard output)")
}

func main() {
	flag.Parse()

	err := run()
	if err != nil {
		log.Fatalln(err)
	}
}

func run() error {
	// Get feathers
	resp, err := http.Get(fmt.Sprintf("https://registry.npmjs.org/feather-icons/-/feather-icons-%s.tgz", featherVersion))
	if err != nil {
		return fmt.Errorf("get feathers: %v", err)
	}
	defer resp.Body.Close()

	tgz, err := os.Create(tgzName)
	if err != nil {
		return fmt.Errorf("create %s: %v", tgzName, err)
	}
	defer os.Remove(tgzName)

	if _, err = io.Copy(tgz, resp.Body); err != nil {
		return fmt.Errorf("copy feathers: %v", err)
	}

	if err := tgz.Close(); err != nil {
		return fmt.Errorf("close %s: %v", tgzName, err)
	}

	tgz, err = os.Open(tgzName)
	if err != nil {
		return fmt.Errorf("open %s: %v", tgzName, err)
	}

	// Unzip
	if err := os.MkdirAll(dirName, os.ModePerm); err != nil {
		return fmt.Errorf("mkdirall %s: %v", dirName, err)
	}
	defer os.RemoveAll(dirName)

	if err := unzip(dirName, tgz); err != nil {
		return fmt.Errorf("unzip feathers: %v", err)
	}

	// Generate
	feathers := make(map[string]string, 0)
	if err := filepath.Walk(dirName+"/package/dist/icons", func(path string, info os.FileInfo, walkErr error) error {
		if walkErr != nil {
			return fmt.Errorf("walk error: %v", walkErr)
		}

		if info.IsDir() {
			return nil
		}

		name := strings.TrimSuffix(filepath.Base(path), ".svg")

		data, err := ioutil.ReadFile(path)
		if err != nil {
			return fmt.Errorf("read file '%s': %v", path, err)
		}

		feathers[name] = string(data)

		return nil
	}); err != nil {
		return fmt.Errorf("walk svgs: %v", err)
	}

	var names []string
	for name := range feathers {
		names = append(names, name)
	}
	sort.Strings(names)

	var buf bytes.Buffer
	fmt.Fprint(&buf, `package feather

import (
	"fmt"
	"html/template"
)

// Icons is a list of all Feather icons
var Icons = []string{"`)

	fmt.Fprintf(&buf, strings.Join(names, `", "`))

	fmt.Fprintf(&buf, `"}

// FeatherIcon represents an SVG node
type FeatherIcon struct {
	xml    string
	width  int
	height int
	style  string
	id     string
	class  string
}

// XML returns the SVG node as an XML string
func (f *FeatherIcon) XML() string {
	return fmt.Sprintf(f.xml, f.width, f.height, f.style, f.id, f.class)
}

// HTML returns the SVG node as an HTML template, safe for use in Go templates
func (f *FeatherIcon) HTML() template.HTML {
	return template.HTML(f.XML())
}

// Size sets the size of a FeatherIcon
// Short for calling Width and Height with the same int
func (f *FeatherIcon) Size(size int) {
	f.Width(size)
	f.Height(size)
}

// Width sets the width of a FeatherIcon
func (f *FeatherIcon) Width(width int) {
	f.width = width
}

// Height sets the height of a FeatherIcon
func (f *FeatherIcon) Height(height int) {
	f.height = height
}

// Style sets the style of a FeatherIcon
func (f *FeatherIcon) Style(style string) {
	f.style = style
}

// Id sets the id of a FeatherIcon
func (f *FeatherIcon) Id(id string) {
	f.id = id
}

// Class sets the class of a FeatherIcon
func (f *FeatherIcon) Class(class string) {
	f.class = class
}

// Icon returns the named Feather SVG node.
// It returns nil if name is not a valid FeatherIcon symbol name.
func Icon(name string) *FeatherIcon {
	switch name {
`)
	for _, name := range names {
		fmt.Fprintf(&buf, "	case %q:\n		return %v()\n", name, kebab(name))
	}
	fmt.Fprint(&buf, `	default:
		return nil
	}
}
`)

	// Write all individual Feather functions.
	for _, name := range names {
		generateAndWriteFeather(&buf, feathers, name)
	}

	var w io.Writer
	switch oFlag {
	case "":
		w = os.Stdout
	default:
		f, err := os.Create(oFlag)
		if err != nil {
			return err
		}
		defer f.Close()
		w = f
	}

	_, err = w.Write(buf.Bytes())
	return err
}

var featherTemplate = template.Must(template.New("feather").Parse(`return &FeatherIcon{
	xml: ` + "`" + `{{.xml}}` + "`" + `,
	width: 16,
	height: 16,
	style: "display: inline-block; vertical-align: text-top;",
}
`))

func generateAndWriteFeather(w io.Writer, feathers map[string]string, name string) {
	fmt.Fprintln(w)
	fmt.Fprintf(w, "// %s returns the %q FeatherIcon.\n", kebab(name), name)
	fmt.Fprintf(w, "func %s() *FeatherIcon {\n", kebab(name))

	data := feathers[name]
	page, err := html.Parse(bytes.NewBufferString(data))
	if err != nil {
		fmt.Println(err)
		return
	}
	node := page.FirstChild.LastChild.FirstChild
	node.Attr[1].Val = "%d"
	node.Attr[2].Val = "%d"

	idxs := make([]int, 0)
	for idx, attr := range node.Attr {
		if attr.Key == "style" || attr.Key == "class" {
			idxs = append(idxs, idx)
		}
	}
	for jdx := len(idxs) - 1; jdx >= 0; jdx-- {
		node.Attr = append(node.Attr[:idxs[jdx]], node.Attr[idxs[jdx]+1:]...)
	}

	node.Attr = append(node.Attr,
		html.Attribute{Key: "style", Val: "%s"},
		html.Attribute{Key: "id", Val: "%s"},
		html.Attribute{Key: "class", Val: "%s"},
	)
	node.InsertBefore(&html.Node{
		FirstChild: &html.Node{
			Type:      html.TextNode,
			Data:      name,
			Namespace: "svg",
		},
		Type:      html.ElementNode,
		DataAtom:  atom.Title,
		Data:      "title",
		Namespace: "svg",
	}, node.FirstChild)

	xml := &strings.Builder{}
	if err := html.Render(xml, node); err != nil {
		fmt.Println(err)
		return
	}

	featherTemplate.Execute(w, map[string]interface{}{
		"xml": xml.String(),
	})
	fmt.Fprintln(w, "}")
}

func kebab(input string) string {
	parts := make([]string, strings.Count(input, "-")+1)
	for idx, part := range strings.Split(input, "-") {
		parts[idx] = strings.Title(part)
	}
	return strings.Join(parts, "")
}

func unzip(dst string, r io.Reader) error {

	gzr, err := gzip.NewReader(r)
	if err != nil {
		return fmt.Errorf("gzip reader: %v", err)
	}
	defer gzr.Close()

	tr := tar.NewReader(gzr)

	for {
		header, err := tr.Next()

		switch {

		// if no more files are found return
		case err == io.EOF:
			return nil

		// return any other error
		case err != nil:
			return err

		// if the header is nil, just skip it (not sure how this happens)
		case header == nil:
			continue
		}

		// the target location where the dir/file should be created
		target := filepath.Join(dst, header.Name)

		if _, err := os.Stat(target); err != nil {
			if err := os.MkdirAll(filepath.Dir(target), 0755); err != nil {
				return err
			}
		}

		f, err := os.OpenFile(target, os.O_CREATE|os.O_RDWR, os.FileMode(header.Mode))
		if err != nil {
			return err
		}

		// copy over contents
		if _, err := io.Copy(f, tr); err != nil {
			return err
		}

		// manually close here after each file operation; deferring would cause each file close
		// to wait until all operations have completed.
		f.Close()
	}
}
