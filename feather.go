package feather

import (
	"fmt"
	"html/template"
)

// Icons is a list of all Feather icons
var Icons = []string{"activity", "airplay", "alert-circle", "alert-octagon", "alert-triangle", "align-center", "align-justify", "align-left", "align-right", "anchor", "aperture", "archive", "arrow-down", "arrow-down-circle", "arrow-down-left", "arrow-down-right", "arrow-left", "arrow-left-circle", "arrow-right", "arrow-right-circle", "arrow-up", "arrow-up-circle", "arrow-up-left", "arrow-up-right", "at-sign", "award", "bar-chart", "bar-chart-2", "battery", "battery-charging", "bell", "bell-off", "bluetooth", "bold", "book", "book-open", "bookmark", "box", "briefcase", "calendar", "camera", "camera-off", "cast", "check", "check-circle", "check-square", "chevron-down", "chevron-left", "chevron-right", "chevron-up", "chevrons-down", "chevrons-left", "chevrons-right", "chevrons-up", "chrome", "circle", "clipboard", "clock", "cloud", "cloud-drizzle", "cloud-lightning", "cloud-off", "cloud-rain", "cloud-snow", "code", "codepen", "codesandbox", "coffee", "columns", "command", "compass", "copy", "corner-down-left", "corner-down-right", "corner-left-down", "corner-left-up", "corner-right-down", "corner-right-up", "corner-up-left", "corner-up-right", "cpu", "credit-card", "crop", "crosshair", "database", "delete", "disc", "dollar-sign", "download", "download-cloud", "droplet", "edit", "edit-2", "edit-3", "external-link", "eye", "eye-off", "facebook", "fast-forward", "feather", "figma", "file", "file-minus", "file-plus", "file-text", "film", "filter", "flag", "folder", "folder-minus", "folder-plus", "framer", "frown", "gift", "git-branch", "git-commit", "git-merge", "git-pull-request", "github", "gitlab", "globe", "grid", "hard-drive", "hash", "headphones", "heart", "help-circle", "hexagon", "home", "image", "inbox", "info", "instagram", "italic", "key", "layers", "layout", "life-buoy", "link", "link-2", "linkedin", "list", "loader", "lock", "log-in", "log-out", "mail", "map", "map-pin", "maximize", "maximize-2", "meh", "menu", "message-circle", "message-square", "mic", "mic-off", "minimize", "minimize-2", "minus", "minus-circle", "minus-square", "monitor", "moon", "more-horizontal", "more-vertical", "mouse-pointer", "move", "music", "navigation", "navigation-2", "octagon", "package", "paperclip", "pause", "pause-circle", "pen-tool", "percent", "phone", "phone-call", "phone-forwarded", "phone-incoming", "phone-missed", "phone-off", "phone-outgoing", "pie-chart", "play", "play-circle", "plus", "plus-circle", "plus-square", "pocket", "power", "printer", "radio", "refresh-ccw", "refresh-cw", "repeat", "rewind", "rotate-ccw", "rotate-cw", "rss", "save", "scissors", "search", "send", "server", "settings", "share", "share-2", "shield", "shield-off", "shopping-bag", "shopping-cart", "shuffle", "sidebar", "skip-back", "skip-forward", "slack", "slash", "sliders", "smartphone", "smile", "speaker", "square", "star", "stop-circle", "sun", "sunrise", "sunset", "tablet", "tag", "target", "terminal", "thermometer", "thumbs-down", "thumbs-up", "toggle-left", "toggle-right", "tool", "trash", "trash-2", "trello", "trending-down", "trending-up", "triangle", "truck", "tv", "twitch", "twitter", "type", "umbrella", "underline", "unlock", "upload", "upload-cloud", "user", "user-check", "user-minus", "user-plus", "user-x", "users", "video", "video-off", "voicemail", "volume", "volume-1", "volume-2", "volume-x", "watch", "wifi", "wifi-off", "wind", "x", "x-circle", "x-octagon", "x-square", "youtube", "zap", "zap-off", "zoom-in", "zoom-out"}

// FeatherIcon represents an SVG node
type FeatherIcon struct {
	xml    string
	width  int
	height int
	style  string
	id     string
	class  string
}

// XML returns the SVG node as an XML string
func (f *FeatherIcon) XML() string {
	return fmt.Sprintf(f.xml, f.width, f.height, f.style, f.id, f.class)
}

// HTML returns the SVG node as an HTML template, safe for use in Go templates
func (f *FeatherIcon) HTML() template.HTML {
	return template.HTML(f.XML())
}

// Size sets the size of a FeatherIcon
// Short for calling Width and Height with the same int
func (f *FeatherIcon) Size(size int) {
	f.Width(size)
	f.Height(size)
}

// Width sets the width of a FeatherIcon
func (f *FeatherIcon) Width(width int) {
	f.width = width
}

// Height sets the height of a FeatherIcon
func (f *FeatherIcon) Height(height int) {
	f.height = height
}

// Style sets the style of a FeatherIcon
func (f *FeatherIcon) Style(style string) {
	f.style = style
}

// Id sets the id of a FeatherIcon
func (f *FeatherIcon) Id(id string) {
	f.id = id
}

// Class sets the class of a FeatherIcon
func (f *FeatherIcon) Class(class string) {
	f.class = class
}

// Icon returns the named Feather SVG node.
// It returns nil if name is not a valid FeatherIcon symbol name.
func Icon(name string) *FeatherIcon {
	switch name {
	case "activity":
		return Activity()
	case "airplay":
		return Airplay()
	case "alert-circle":
		return AlertCircle()
	case "alert-octagon":
		return AlertOctagon()
	case "alert-triangle":
		return AlertTriangle()
	case "align-center":
		return AlignCenter()
	case "align-justify":
		return AlignJustify()
	case "align-left":
		return AlignLeft()
	case "align-right":
		return AlignRight()
	case "anchor":
		return Anchor()
	case "aperture":
		return Aperture()
	case "archive":
		return Archive()
	case "arrow-down":
		return ArrowDown()
	case "arrow-down-circle":
		return ArrowDownCircle()
	case "arrow-down-left":
		return ArrowDownLeft()
	case "arrow-down-right":
		return ArrowDownRight()
	case "arrow-left":
		return ArrowLeft()
	case "arrow-left-circle":
		return ArrowLeftCircle()
	case "arrow-right":
		return ArrowRight()
	case "arrow-right-circle":
		return ArrowRightCircle()
	case "arrow-up":
		return ArrowUp()
	case "arrow-up-circle":
		return ArrowUpCircle()
	case "arrow-up-left":
		return ArrowUpLeft()
	case "arrow-up-right":
		return ArrowUpRight()
	case "at-sign":
		return AtSign()
	case "award":
		return Award()
	case "bar-chart":
		return BarChart()
	case "bar-chart-2":
		return BarChart2()
	case "battery":
		return Battery()
	case "battery-charging":
		return BatteryCharging()
	case "bell":
		return Bell()
	case "bell-off":
		return BellOff()
	case "bluetooth":
		return Bluetooth()
	case "bold":
		return Bold()
	case "book":
		return Book()
	case "book-open":
		return BookOpen()
	case "bookmark":
		return Bookmark()
	case "box":
		return Box()
	case "briefcase":
		return Briefcase()
	case "calendar":
		return Calendar()
	case "camera":
		return Camera()
	case "camera-off":
		return CameraOff()
	case "cast":
		return Cast()
	case "check":
		return Check()
	case "check-circle":
		return CheckCircle()
	case "check-square":
		return CheckSquare()
	case "chevron-down":
		return ChevronDown()
	case "chevron-left":
		return ChevronLeft()
	case "chevron-right":
		return ChevronRight()
	case "chevron-up":
		return ChevronUp()
	case "chevrons-down":
		return ChevronsDown()
	case "chevrons-left":
		return ChevronsLeft()
	case "chevrons-right":
		return ChevronsRight()
	case "chevrons-up":
		return ChevronsUp()
	case "chrome":
		return Chrome()
	case "circle":
		return Circle()
	case "clipboard":
		return Clipboard()
	case "clock":
		return Clock()
	case "cloud":
		return Cloud()
	case "cloud-drizzle":
		return CloudDrizzle()
	case "cloud-lightning":
		return CloudLightning()
	case "cloud-off":
		return CloudOff()
	case "cloud-rain":
		return CloudRain()
	case "cloud-snow":
		return CloudSnow()
	case "code":
		return Code()
	case "codepen":
		return Codepen()
	case "codesandbox":
		return Codesandbox()
	case "coffee":
		return Coffee()
	case "columns":
		return Columns()
	case "command":
		return Command()
	case "compass":
		return Compass()
	case "copy":
		return Copy()
	case "corner-down-left":
		return CornerDownLeft()
	case "corner-down-right":
		return CornerDownRight()
	case "corner-left-down":
		return CornerLeftDown()
	case "corner-left-up":
		return CornerLeftUp()
	case "corner-right-down":
		return CornerRightDown()
	case "corner-right-up":
		return CornerRightUp()
	case "corner-up-left":
		return CornerUpLeft()
	case "corner-up-right":
		return CornerUpRight()
	case "cpu":
		return Cpu()
	case "credit-card":
		return CreditCard()
	case "crop":
		return Crop()
	case "crosshair":
		return Crosshair()
	case "database":
		return Database()
	case "delete":
		return Delete()
	case "disc":
		return Disc()
	case "dollar-sign":
		return DollarSign()
	case "download":
		return Download()
	case "download-cloud":
		return DownloadCloud()
	case "droplet":
		return Droplet()
	case "edit":
		return Edit()
	case "edit-2":
		return Edit2()
	case "edit-3":
		return Edit3()
	case "external-link":
		return ExternalLink()
	case "eye":
		return Eye()
	case "eye-off":
		return EyeOff()
	case "facebook":
		return Facebook()
	case "fast-forward":
		return FastForward()
	case "feather":
		return Feather()
	case "figma":
		return Figma()
	case "file":
		return File()
	case "file-minus":
		return FileMinus()
	case "file-plus":
		return FilePlus()
	case "file-text":
		return FileText()
	case "film":
		return Film()
	case "filter":
		return Filter()
	case "flag":
		return Flag()
	case "folder":
		return Folder()
	case "folder-minus":
		return FolderMinus()
	case "folder-plus":
		return FolderPlus()
	case "framer":
		return Framer()
	case "frown":
		return Frown()
	case "gift":
		return Gift()
	case "git-branch":
		return GitBranch()
	case "git-commit":
		return GitCommit()
	case "git-merge":
		return GitMerge()
	case "git-pull-request":
		return GitPullRequest()
	case "github":
		return Github()
	case "gitlab":
		return Gitlab()
	case "globe":
		return Globe()
	case "grid":
		return Grid()
	case "hard-drive":
		return HardDrive()
	case "hash":
		return Hash()
	case "headphones":
		return Headphones()
	case "heart":
		return Heart()
	case "help-circle":
		return HelpCircle()
	case "hexagon":
		return Hexagon()
	case "home":
		return Home()
	case "image":
		return Image()
	case "inbox":
		return Inbox()
	case "info":
		return Info()
	case "instagram":
		return Instagram()
	case "italic":
		return Italic()
	case "key":
		return Key()
	case "layers":
		return Layers()
	case "layout":
		return Layout()
	case "life-buoy":
		return LifeBuoy()
	case "link":
		return Link()
	case "link-2":
		return Link2()
	case "linkedin":
		return Linkedin()
	case "list":
		return List()
	case "loader":
		return Loader()
	case "lock":
		return Lock()
	case "log-in":
		return LogIn()
	case "log-out":
		return LogOut()
	case "mail":
		return Mail()
	case "map":
		return Map()
	case "map-pin":
		return MapPin()
	case "maximize":
		return Maximize()
	case "maximize-2":
		return Maximize2()
	case "meh":
		return Meh()
	case "menu":
		return Menu()
	case "message-circle":
		return MessageCircle()
	case "message-square":
		return MessageSquare()
	case "mic":
		return Mic()
	case "mic-off":
		return MicOff()
	case "minimize":
		return Minimize()
	case "minimize-2":
		return Minimize2()
	case "minus":
		return Minus()
	case "minus-circle":
		return MinusCircle()
	case "minus-square":
		return MinusSquare()
	case "monitor":
		return Monitor()
	case "moon":
		return Moon()
	case "more-horizontal":
		return MoreHorizontal()
	case "more-vertical":
		return MoreVertical()
	case "mouse-pointer":
		return MousePointer()
	case "move":
		return Move()
	case "music":
		return Music()
	case "navigation":
		return Navigation()
	case "navigation-2":
		return Navigation2()
	case "octagon":
		return Octagon()
	case "package":
		return Package()
	case "paperclip":
		return Paperclip()
	case "pause":
		return Pause()
	case "pause-circle":
		return PauseCircle()
	case "pen-tool":
		return PenTool()
	case "percent":
		return Percent()
	case "phone":
		return Phone()
	case "phone-call":
		return PhoneCall()
	case "phone-forwarded":
		return PhoneForwarded()
	case "phone-incoming":
		return PhoneIncoming()
	case "phone-missed":
		return PhoneMissed()
	case "phone-off":
		return PhoneOff()
	case "phone-outgoing":
		return PhoneOutgoing()
	case "pie-chart":
		return PieChart()
	case "play":
		return Play()
	case "play-circle":
		return PlayCircle()
	case "plus":
		return Plus()
	case "plus-circle":
		return PlusCircle()
	case "plus-square":
		return PlusSquare()
	case "pocket":
		return Pocket()
	case "power":
		return Power()
	case "printer":
		return Printer()
	case "radio":
		return Radio()
	case "refresh-ccw":
		return RefreshCcw()
	case "refresh-cw":
		return RefreshCw()
	case "repeat":
		return Repeat()
	case "rewind":
		return Rewind()
	case "rotate-ccw":
		return RotateCcw()
	case "rotate-cw":
		return RotateCw()
	case "rss":
		return Rss()
	case "save":
		return Save()
	case "scissors":
		return Scissors()
	case "search":
		return Search()
	case "send":
		return Send()
	case "server":
		return Server()
	case "settings":
		return Settings()
	case "share":
		return Share()
	case "share-2":
		return Share2()
	case "shield":
		return Shield()
	case "shield-off":
		return ShieldOff()
	case "shopping-bag":
		return ShoppingBag()
	case "shopping-cart":
		return ShoppingCart()
	case "shuffle":
		return Shuffle()
	case "sidebar":
		return Sidebar()
	case "skip-back":
		return SkipBack()
	case "skip-forward":
		return SkipForward()
	case "slack":
		return Slack()
	case "slash":
		return Slash()
	case "sliders":
		return Sliders()
	case "smartphone":
		return Smartphone()
	case "smile":
		return Smile()
	case "speaker":
		return Speaker()
	case "square":
		return Square()
	case "star":
		return Star()
	case "stop-circle":
		return StopCircle()
	case "sun":
		return Sun()
	case "sunrise":
		return Sunrise()
	case "sunset":
		return Sunset()
	case "tablet":
		return Tablet()
	case "tag":
		return Tag()
	case "target":
		return Target()
	case "terminal":
		return Terminal()
	case "thermometer":
		return Thermometer()
	case "thumbs-down":
		return ThumbsDown()
	case "thumbs-up":
		return ThumbsUp()
	case "toggle-left":
		return ToggleLeft()
	case "toggle-right":
		return ToggleRight()
	case "tool":
		return Tool()
	case "trash":
		return Trash()
	case "trash-2":
		return Trash2()
	case "trello":
		return Trello()
	case "trending-down":
		return TrendingDown()
	case "trending-up":
		return TrendingUp()
	case "triangle":
		return Triangle()
	case "truck":
		return Truck()
	case "tv":
		return Tv()
	case "twitch":
		return Twitch()
	case "twitter":
		return Twitter()
	case "type":
		return Type()
	case "umbrella":
		return Umbrella()
	case "underline":
		return Underline()
	case "unlock":
		return Unlock()
	case "upload":
		return Upload()
	case "upload-cloud":
		return UploadCloud()
	case "user":
		return User()
	case "user-check":
		return UserCheck()
	case "user-minus":
		return UserMinus()
	case "user-plus":
		return UserPlus()
	case "user-x":
		return UserX()
	case "users":
		return Users()
	case "video":
		return Video()
	case "video-off":
		return VideoOff()
	case "voicemail":
		return Voicemail()
	case "volume":
		return Volume()
	case "volume-1":
		return Volume1()
	case "volume-2":
		return Volume2()
	case "volume-x":
		return VolumeX()
	case "watch":
		return Watch()
	case "wifi":
		return Wifi()
	case "wifi-off":
		return WifiOff()
	case "wind":
		return Wind()
	case "x":
		return X()
	case "x-circle":
		return XCircle()
	case "x-octagon":
		return XOctagon()
	case "x-square":
		return XSquare()
	case "youtube":
		return Youtube()
	case "zap":
		return Zap()
	case "zap-off":
		return ZapOff()
	case "zoom-in":
		return ZoomIn()
	case "zoom-out":
		return ZoomOut()
	default:
		return nil
	}
}

// Activity returns the "activity" FeatherIcon.
func Activity() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>activity</title><polyline points="22 12 18 12 15 21 9 3 6 12 2 12"></polyline></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Airplay returns the "airplay" FeatherIcon.
func Airplay() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>airplay</title><path d="M5 17H4a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2h-1"></path><polygon points="12 15 17 21 7 21 12 15"></polygon></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// AlertCircle returns the "alert-circle" FeatherIcon.
func AlertCircle() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>alert-circle</title><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="8" x2="12" y2="12"></line><line x1="12" y1="16" x2="12.01" y2="16"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// AlertOctagon returns the "alert-octagon" FeatherIcon.
func AlertOctagon() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>alert-octagon</title><polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon><line x1="12" y1="8" x2="12" y2="12"></line><line x1="12" y1="16" x2="12.01" y2="16"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// AlertTriangle returns the "alert-triangle" FeatherIcon.
func AlertTriangle() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>alert-triangle</title><path d="M10.29 3.86L1.82 18a2 2 0 0 0 1.71 3h16.94a2 2 0 0 0 1.71-3L13.71 3.86a2 2 0 0 0-3.42 0z"></path><line x1="12" y1="9" x2="12" y2="13"></line><line x1="12" y1="17" x2="12.01" y2="17"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// AlignCenter returns the "align-center" FeatherIcon.
func AlignCenter() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>align-center</title><line x1="18" y1="10" x2="6" y2="10"></line><line x1="21" y1="6" x2="3" y2="6"></line><line x1="21" y1="14" x2="3" y2="14"></line><line x1="18" y1="18" x2="6" y2="18"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// AlignJustify returns the "align-justify" FeatherIcon.
func AlignJustify() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>align-justify</title><line x1="21" y1="10" x2="3" y2="10"></line><line x1="21" y1="6" x2="3" y2="6"></line><line x1="21" y1="14" x2="3" y2="14"></line><line x1="21" y1="18" x2="3" y2="18"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// AlignLeft returns the "align-left" FeatherIcon.
func AlignLeft() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>align-left</title><line x1="17" y1="10" x2="3" y2="10"></line><line x1="21" y1="6" x2="3" y2="6"></line><line x1="21" y1="14" x2="3" y2="14"></line><line x1="17" y1="18" x2="3" y2="18"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// AlignRight returns the "align-right" FeatherIcon.
func AlignRight() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>align-right</title><line x1="21" y1="10" x2="7" y2="10"></line><line x1="21" y1="6" x2="3" y2="6"></line><line x1="21" y1="14" x2="3" y2="14"></line><line x1="21" y1="18" x2="7" y2="18"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Anchor returns the "anchor" FeatherIcon.
func Anchor() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>anchor</title><circle cx="12" cy="5" r="3"></circle><line x1="12" y1="22" x2="12" y2="8"></line><path d="M5 12H2a10 10 0 0 0 20 0h-3"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Aperture returns the "aperture" FeatherIcon.
func Aperture() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>aperture</title><circle cx="12" cy="12" r="10"></circle><line x1="14.31" y1="8" x2="20.05" y2="17.94"></line><line x1="9.69" y1="8" x2="21.17" y2="8"></line><line x1="7.38" y1="12" x2="13.12" y2="2.06"></line><line x1="9.69" y1="16" x2="3.95" y2="6.06"></line><line x1="14.31" y1="16" x2="2.83" y2="16"></line><line x1="16.62" y1="12" x2="10.88" y2="21.94"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Archive returns the "archive" FeatherIcon.
func Archive() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>archive</title><polyline points="21 8 21 21 3 21 3 8"></polyline><rect x="1" y="3" width="22" height="5"></rect><line x1="10" y1="12" x2="14" y2="12"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// ArrowDown returns the "arrow-down" FeatherIcon.
func ArrowDown() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>arrow-down</title><line x1="12" y1="5" x2="12" y2="19"></line><polyline points="19 12 12 19 5 12"></polyline></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// ArrowDownCircle returns the "arrow-down-circle" FeatherIcon.
func ArrowDownCircle() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>arrow-down-circle</title><circle cx="12" cy="12" r="10"></circle><polyline points="8 12 12 16 16 12"></polyline><line x1="12" y1="8" x2="12" y2="16"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// ArrowDownLeft returns the "arrow-down-left" FeatherIcon.
func ArrowDownLeft() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>arrow-down-left</title><line x1="17" y1="7" x2="7" y2="17"></line><polyline points="17 17 7 17 7 7"></polyline></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// ArrowDownRight returns the "arrow-down-right" FeatherIcon.
func ArrowDownRight() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>arrow-down-right</title><line x1="7" y1="7" x2="17" y2="17"></line><polyline points="17 7 17 17 7 17"></polyline></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// ArrowLeft returns the "arrow-left" FeatherIcon.
func ArrowLeft() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>arrow-left</title><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// ArrowLeftCircle returns the "arrow-left-circle" FeatherIcon.
func ArrowLeftCircle() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>arrow-left-circle</title><circle cx="12" cy="12" r="10"></circle><polyline points="12 8 8 12 12 16"></polyline><line x1="16" y1="12" x2="8" y2="12"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// ArrowRight returns the "arrow-right" FeatherIcon.
func ArrowRight() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>arrow-right</title><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// ArrowRightCircle returns the "arrow-right-circle" FeatherIcon.
func ArrowRightCircle() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>arrow-right-circle</title><circle cx="12" cy="12" r="10"></circle><polyline points="12 16 16 12 12 8"></polyline><line x1="8" y1="12" x2="16" y2="12"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// ArrowUp returns the "arrow-up" FeatherIcon.
func ArrowUp() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>arrow-up</title><line x1="12" y1="19" x2="12" y2="5"></line><polyline points="5 12 12 5 19 12"></polyline></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// ArrowUpCircle returns the "arrow-up-circle" FeatherIcon.
func ArrowUpCircle() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>arrow-up-circle</title><circle cx="12" cy="12" r="10"></circle><polyline points="16 12 12 8 8 12"></polyline><line x1="12" y1="16" x2="12" y2="8"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// ArrowUpLeft returns the "arrow-up-left" FeatherIcon.
func ArrowUpLeft() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>arrow-up-left</title><line x1="17" y1="17" x2="7" y2="7"></line><polyline points="7 17 7 7 17 7"></polyline></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// ArrowUpRight returns the "arrow-up-right" FeatherIcon.
func ArrowUpRight() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>arrow-up-right</title><line x1="7" y1="17" x2="17" y2="7"></line><polyline points="7 7 17 7 17 17"></polyline></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// AtSign returns the "at-sign" FeatherIcon.
func AtSign() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>at-sign</title><circle cx="12" cy="12" r="4"></circle><path d="M16 8v5a3 3 0 0 0 6 0v-1a10 10 0 1 0-3.92 7.94"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Award returns the "award" FeatherIcon.
func Award() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>award</title><circle cx="12" cy="8" r="7"></circle><polyline points="8.21 13.89 7 23 12 20 17 23 15.79 13.88"></polyline></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// BarChart returns the "bar-chart" FeatherIcon.
func BarChart() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>bar-chart</title><line x1="12" y1="20" x2="12" y2="10"></line><line x1="18" y1="20" x2="18" y2="4"></line><line x1="6" y1="20" x2="6" y2="16"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// BarChart2 returns the "bar-chart-2" FeatherIcon.
func BarChart2() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>bar-chart-2</title><line x1="18" y1="20" x2="18" y2="10"></line><line x1="12" y1="20" x2="12" y2="4"></line><line x1="6" y1="20" x2="6" y2="14"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Battery returns the "battery" FeatherIcon.
func Battery() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>battery</title><rect x="1" y="6" width="18" height="12" rx="2" ry="2"></rect><line x1="23" y1="13" x2="23" y2="11"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// BatteryCharging returns the "battery-charging" FeatherIcon.
func BatteryCharging() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>battery-charging</title><path d="M5 18H3a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h3.19M15 6h2a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2h-3.19"></path><line x1="23" y1="13" x2="23" y2="11"></line><polyline points="11 6 7 12 13 12 9 18"></polyline></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Bell returns the "bell" FeatherIcon.
func Bell() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>bell</title><path d="M18 8A6 6 0 0 0 6 8c0 7-3 9-3 9h18s-3-2-3-9"></path><path d="M13.73 21a2 2 0 0 1-3.46 0"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// BellOff returns the "bell-off" FeatherIcon.
func BellOff() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>bell-off</title><path d="M13.73 21a2 2 0 0 1-3.46 0"></path><path d="M18.63 13A17.89 17.89 0 0 1 18 8"></path><path d="M6.26 6.26A5.86 5.86 0 0 0 6 8c0 7-3 9-3 9h14"></path><path d="M18 8a6 6 0 0 0-9.33-5"></path><line x1="1" y1="1" x2="23" y2="23"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Bluetooth returns the "bluetooth" FeatherIcon.
func Bluetooth() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>bluetooth</title><polyline points="6.5 6.5 17.5 17.5 12 23 12 1 17.5 6.5 6.5 17.5"></polyline></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Bold returns the "bold" FeatherIcon.
func Bold() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>bold</title><path d="M6 4h8a4 4 0 0 1 4 4 4 4 0 0 1-4 4H6z"></path><path d="M6 12h9a4 4 0 0 1 4 4 4 4 0 0 1-4 4H6z"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Book returns the "book" FeatherIcon.
func Book() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>book</title><path d="M4 19.5A2.5 2.5 0 0 1 6.5 17H20"></path><path d="M6.5 2H20v20H6.5A2.5 2.5 0 0 1 4 19.5v-15A2.5 2.5 0 0 1 6.5 2z"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// BookOpen returns the "book-open" FeatherIcon.
func BookOpen() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>book-open</title><path d="M2 3h6a4 4 0 0 1 4 4v14a3 3 0 0 0-3-3H2z"></path><path d="M22 3h-6a4 4 0 0 0-4 4v14a3 3 0 0 1 3-3h7z"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Bookmark returns the "bookmark" FeatherIcon.
func Bookmark() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>bookmark</title><path d="M19 21l-7-5-7 5V5a2 2 0 0 1 2-2h10a2 2 0 0 1 2 2z"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Box returns the "box" FeatherIcon.
func Box() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>box</title><path d="M21 16V8a2 2 0 0 0-1-1.73l-7-4a2 2 0 0 0-2 0l-7 4A2 2 0 0 0 3 8v8a2 2 0 0 0 1 1.73l7 4a2 2 0 0 0 2 0l7-4A2 2 0 0 0 21 16z"></path><polyline points="3.27 6.96 12 12.01 20.73 6.96"></polyline><line x1="12" y1="22.08" x2="12" y2="12"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Briefcase returns the "briefcase" FeatherIcon.
func Briefcase() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>briefcase</title><rect x="2" y="7" width="20" height="14" rx="2" ry="2"></rect><path d="M16 21V5a2 2 0 0 0-2-2h-4a2 2 0 0 0-2 2v16"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Calendar returns the "calendar" FeatherIcon.
func Calendar() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>calendar</title><rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect><line x1="16" y1="2" x2="16" y2="6"></line><line x1="8" y1="2" x2="8" y2="6"></line><line x1="3" y1="10" x2="21" y2="10"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Camera returns the "camera" FeatherIcon.
func Camera() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>camera</title><path d="M23 19a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h4l2-3h6l2 3h4a2 2 0 0 1 2 2z"></path><circle cx="12" cy="13" r="4"></circle></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// CameraOff returns the "camera-off" FeatherIcon.
func CameraOff() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>camera-off</title><line x1="1" y1="1" x2="23" y2="23"></line><path d="M21 21H3a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h3m3-3h6l2 3h4a2 2 0 0 1 2 2v9.34m-7.72-2.06a4 4 0 1 1-5.56-5.56"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Cast returns the "cast" FeatherIcon.
func Cast() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>cast</title><path d="M2 16.1A5 5 0 0 1 5.9 20M2 12.05A9 9 0 0 1 9.95 20M2 8V6a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2h-6"></path><line x1="2" y1="20" x2="2.01" y2="20"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Check returns the "check" FeatherIcon.
func Check() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>check</title><polyline points="20 6 9 17 4 12"></polyline></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// CheckCircle returns the "check-circle" FeatherIcon.
func CheckCircle() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>check-circle</title><path d="M22 11.08V12a10 10 0 1 1-5.93-9.14"></path><polyline points="22 4 12 14.01 9 11.01"></polyline></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// CheckSquare returns the "check-square" FeatherIcon.
func CheckSquare() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>check-square</title><polyline points="9 11 12 14 22 4"></polyline><path d="M21 12v7a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// ChevronDown returns the "chevron-down" FeatherIcon.
func ChevronDown() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>chevron-down</title><polyline points="6 9 12 15 18 9"></polyline></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// ChevronLeft returns the "chevron-left" FeatherIcon.
func ChevronLeft() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>chevron-left</title><polyline points="15 18 9 12 15 6"></polyline></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// ChevronRight returns the "chevron-right" FeatherIcon.
func ChevronRight() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>chevron-right</title><polyline points="9 18 15 12 9 6"></polyline></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// ChevronUp returns the "chevron-up" FeatherIcon.
func ChevronUp() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>chevron-up</title><polyline points="18 15 12 9 6 15"></polyline></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// ChevronsDown returns the "chevrons-down" FeatherIcon.
func ChevronsDown() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>chevrons-down</title><polyline points="7 13 12 18 17 13"></polyline><polyline points="7 6 12 11 17 6"></polyline></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// ChevronsLeft returns the "chevrons-left" FeatherIcon.
func ChevronsLeft() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>chevrons-left</title><polyline points="11 17 6 12 11 7"></polyline><polyline points="18 17 13 12 18 7"></polyline></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// ChevronsRight returns the "chevrons-right" FeatherIcon.
func ChevronsRight() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>chevrons-right</title><polyline points="13 17 18 12 13 7"></polyline><polyline points="6 17 11 12 6 7"></polyline></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// ChevronsUp returns the "chevrons-up" FeatherIcon.
func ChevronsUp() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>chevrons-up</title><polyline points="17 11 12 6 7 11"></polyline><polyline points="17 18 12 13 7 18"></polyline></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Chrome returns the "chrome" FeatherIcon.
func Chrome() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>chrome</title><circle cx="12" cy="12" r="10"></circle><circle cx="12" cy="12" r="4"></circle><line x1="21.17" y1="8" x2="12" y2="8"></line><line x1="3.95" y1="6.06" x2="8.54" y2="14"></line><line x1="10.88" y1="21.94" x2="15.46" y2="14"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Circle returns the "circle" FeatherIcon.
func Circle() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>circle</title><circle cx="12" cy="12" r="10"></circle></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Clipboard returns the "clipboard" FeatherIcon.
func Clipboard() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>clipboard</title><path d="M16 4h2a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h2"></path><rect x="8" y="2" width="8" height="4" rx="1" ry="1"></rect></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Clock returns the "clock" FeatherIcon.
func Clock() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>clock</title><circle cx="12" cy="12" r="10"></circle><polyline points="12 6 12 12 16 14"></polyline></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Cloud returns the "cloud" FeatherIcon.
func Cloud() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>cloud</title><path d="M18 10h-1.26A8 8 0 1 0 9 20h9a5 5 0 0 0 0-10z"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// CloudDrizzle returns the "cloud-drizzle" FeatherIcon.
func CloudDrizzle() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>cloud-drizzle</title><line x1="8" y1="19" x2="8" y2="21"></line><line x1="8" y1="13" x2="8" y2="15"></line><line x1="16" y1="19" x2="16" y2="21"></line><line x1="16" y1="13" x2="16" y2="15"></line><line x1="12" y1="21" x2="12" y2="23"></line><line x1="12" y1="15" x2="12" y2="17"></line><path d="M20 16.58A5 5 0 0 0 18 7h-1.26A8 8 0 1 0 4 15.25"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// CloudLightning returns the "cloud-lightning" FeatherIcon.
func CloudLightning() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>cloud-lightning</title><path d="M19 16.9A5 5 0 0 0 18 7h-1.26a8 8 0 1 0-11.62 9"></path><polyline points="13 11 9 17 15 17 11 23"></polyline></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// CloudOff returns the "cloud-off" FeatherIcon.
func CloudOff() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>cloud-off</title><path d="M22.61 16.95A5 5 0 0 0 18 10h-1.26a8 8 0 0 0-7.05-6M5 5a8 8 0 0 0 4 15h9a5 5 0 0 0 1.7-.3"></path><line x1="1" y1="1" x2="23" y2="23"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// CloudRain returns the "cloud-rain" FeatherIcon.
func CloudRain() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>cloud-rain</title><line x1="16" y1="13" x2="16" y2="21"></line><line x1="8" y1="13" x2="8" y2="21"></line><line x1="12" y1="15" x2="12" y2="23"></line><path d="M20 16.58A5 5 0 0 0 18 7h-1.26A8 8 0 1 0 4 15.25"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// CloudSnow returns the "cloud-snow" FeatherIcon.
func CloudSnow() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>cloud-snow</title><path d="M20 17.58A5 5 0 0 0 18 8h-1.26A8 8 0 1 0 4 16.25"></path><line x1="8" y1="16" x2="8.01" y2="16"></line><line x1="8" y1="20" x2="8.01" y2="20"></line><line x1="12" y1="18" x2="12.01" y2="18"></line><line x1="12" y1="22" x2="12.01" y2="22"></line><line x1="16" y1="16" x2="16.01" y2="16"></line><line x1="16" y1="20" x2="16.01" y2="20"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Code returns the "code" FeatherIcon.
func Code() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>code</title><polyline points="16 18 22 12 16 6"></polyline><polyline points="8 6 2 12 8 18"></polyline></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Codepen returns the "codepen" FeatherIcon.
func Codepen() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>codepen</title><polygon points="12 2 22 8.5 22 15.5 12 22 2 15.5 2 8.5 12 2"></polygon><line x1="12" y1="22" x2="12" y2="15.5"></line><polyline points="22 8.5 12 15.5 2 8.5"></polyline><polyline points="2 15.5 12 8.5 22 15.5"></polyline><line x1="12" y1="2" x2="12" y2="8.5"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Codesandbox returns the "codesandbox" FeatherIcon.
func Codesandbox() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>codesandbox</title><path d="M21 16V8a2 2 0 0 0-1-1.73l-7-4a2 2 0 0 0-2 0l-7 4A2 2 0 0 0 3 8v8a2 2 0 0 0 1 1.73l7 4a2 2 0 0 0 2 0l7-4A2 2 0 0 0 21 16z"></path><polyline points="7.5 4.21 12 6.81 16.5 4.21"></polyline><polyline points="7.5 19.79 7.5 14.6 3 12"></polyline><polyline points="21 12 16.5 14.6 16.5 19.79"></polyline><polyline points="3.27 6.96 12 12.01 20.73 6.96"></polyline><line x1="12" y1="22.08" x2="12" y2="12"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Coffee returns the "coffee" FeatherIcon.
func Coffee() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>coffee</title><path d="M18 8h1a4 4 0 0 1 0 8h-1"></path><path d="M2 8h16v9a4 4 0 0 1-4 4H6a4 4 0 0 1-4-4V8z"></path><line x1="6" y1="1" x2="6" y2="4"></line><line x1="10" y1="1" x2="10" y2="4"></line><line x1="14" y1="1" x2="14" y2="4"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Columns returns the "columns" FeatherIcon.
func Columns() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>columns</title><path d="M12 3h7a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2h-7m0-18H5a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h7m0-18v18"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Command returns the "command" FeatherIcon.
func Command() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>command</title><path d="M18 3a3 3 0 0 0-3 3v12a3 3 0 0 0 3 3 3 3 0 0 0 3-3 3 3 0 0 0-3-3H6a3 3 0 0 0-3 3 3 3 0 0 0 3 3 3 3 0 0 0 3-3V6a3 3 0 0 0-3-3 3 3 0 0 0-3 3 3 3 0 0 0 3 3h12a3 3 0 0 0 3-3 3 3 0 0 0-3-3z"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Compass returns the "compass" FeatherIcon.
func Compass() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>compass</title><circle cx="12" cy="12" r="10"></circle><polygon points="16.24 7.76 14.12 14.12 7.76 16.24 9.88 9.88 16.24 7.76"></polygon></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Copy returns the "copy" FeatherIcon.
func Copy() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>copy</title><rect x="9" y="9" width="13" height="13" rx="2" ry="2"></rect><path d="M5 15H4a2 2 0 0 1-2-2V4a2 2 0 0 1 2-2h9a2 2 0 0 1 2 2v1"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// CornerDownLeft returns the "corner-down-left" FeatherIcon.
func CornerDownLeft() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>corner-down-left</title><polyline points="9 10 4 15 9 20"></polyline><path d="M20 4v7a4 4 0 0 1-4 4H4"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// CornerDownRight returns the "corner-down-right" FeatherIcon.
func CornerDownRight() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>corner-down-right</title><polyline points="15 10 20 15 15 20"></polyline><path d="M4 4v7a4 4 0 0 0 4 4h12"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// CornerLeftDown returns the "corner-left-down" FeatherIcon.
func CornerLeftDown() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>corner-left-down</title><polyline points="14 15 9 20 4 15"></polyline><path d="M20 4h-7a4 4 0 0 0-4 4v12"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// CornerLeftUp returns the "corner-left-up" FeatherIcon.
func CornerLeftUp() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>corner-left-up</title><polyline points="14 9 9 4 4 9"></polyline><path d="M20 20h-7a4 4 0 0 1-4-4V4"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// CornerRightDown returns the "corner-right-down" FeatherIcon.
func CornerRightDown() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>corner-right-down</title><polyline points="10 15 15 20 20 15"></polyline><path d="M4 4h7a4 4 0 0 1 4 4v12"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// CornerRightUp returns the "corner-right-up" FeatherIcon.
func CornerRightUp() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>corner-right-up</title><polyline points="10 9 15 4 20 9"></polyline><path d="M4 20h7a4 4 0 0 0 4-4V4"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// CornerUpLeft returns the "corner-up-left" FeatherIcon.
func CornerUpLeft() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>corner-up-left</title><polyline points="9 14 4 9 9 4"></polyline><path d="M20 20v-7a4 4 0 0 0-4-4H4"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// CornerUpRight returns the "corner-up-right" FeatherIcon.
func CornerUpRight() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>corner-up-right</title><polyline points="15 14 20 9 15 4"></polyline><path d="M4 20v-7a4 4 0 0 1 4-4h12"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Cpu returns the "cpu" FeatherIcon.
func Cpu() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>cpu</title><rect x="4" y="4" width="16" height="16" rx="2" ry="2"></rect><rect x="9" y="9" width="6" height="6"></rect><line x1="9" y1="1" x2="9" y2="4"></line><line x1="15" y1="1" x2="15" y2="4"></line><line x1="9" y1="20" x2="9" y2="23"></line><line x1="15" y1="20" x2="15" y2="23"></line><line x1="20" y1="9" x2="23" y2="9"></line><line x1="20" y1="14" x2="23" y2="14"></line><line x1="1" y1="9" x2="4" y2="9"></line><line x1="1" y1="14" x2="4" y2="14"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// CreditCard returns the "credit-card" FeatherIcon.
func CreditCard() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>credit-card</title><rect x="1" y="4" width="22" height="16" rx="2" ry="2"></rect><line x1="1" y1="10" x2="23" y2="10"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Crop returns the "crop" FeatherIcon.
func Crop() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>crop</title><path d="M6.13 1L6 16a2 2 0 0 0 2 2h15"></path><path d="M1 6.13L16 6a2 2 0 0 1 2 2v15"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Crosshair returns the "crosshair" FeatherIcon.
func Crosshair() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>crosshair</title><circle cx="12" cy="12" r="10"></circle><line x1="22" y1="12" x2="18" y2="12"></line><line x1="6" y1="12" x2="2" y2="12"></line><line x1="12" y1="6" x2="12" y2="2"></line><line x1="12" y1="22" x2="12" y2="18"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Database returns the "database" FeatherIcon.
func Database() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>database</title><ellipse cx="12" cy="5" rx="9" ry="3"></ellipse><path d="M21 12c0 1.66-4 3-9 3s-9-1.34-9-3"></path><path d="M3 5v14c0 1.66 4 3 9 3s9-1.34 9-3V5"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Delete returns the "delete" FeatherIcon.
func Delete() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>delete</title><path d="M21 4H8l-7 8 7 8h13a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2z"></path><line x1="18" y1="9" x2="12" y2="15"></line><line x1="12" y1="9" x2="18" y2="15"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Disc returns the "disc" FeatherIcon.
func Disc() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>disc</title><circle cx="12" cy="12" r="10"></circle><circle cx="12" cy="12" r="3"></circle></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// DollarSign returns the "dollar-sign" FeatherIcon.
func DollarSign() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>dollar-sign</title><line x1="12" y1="1" x2="12" y2="23"></line><path d="M17 5H9.5a3.5 3.5 0 0 0 0 7h5a3.5 3.5 0 0 1 0 7H6"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Download returns the "download" FeatherIcon.
func Download() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>download</title><path d="M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4"></path><polyline points="7 10 12 15 17 10"></polyline><line x1="12" y1="15" x2="12" y2="3"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// DownloadCloud returns the "download-cloud" FeatherIcon.
func DownloadCloud() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>download-cloud</title><polyline points="8 17 12 21 16 17"></polyline><line x1="12" y1="12" x2="12" y2="21"></line><path d="M20.88 18.09A5 5 0 0 0 18 9h-1.26A8 8 0 1 0 3 16.29"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Droplet returns the "droplet" FeatherIcon.
func Droplet() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>droplet</title><path d="M12 2.69l5.66 5.66a8 8 0 1 1-11.31 0z"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Edit returns the "edit" FeatherIcon.
func Edit() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>edit</title><path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path><path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Edit2 returns the "edit-2" FeatherIcon.
func Edit2() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>edit-2</title><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Edit3 returns the "edit-3" FeatherIcon.
func Edit3() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>edit-3</title><path d="M12 20h9"></path><path d="M16.5 3.5a2.121 2.121 0 0 1 3 3L7 19l-4 1 1-4L16.5 3.5z"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// ExternalLink returns the "external-link" FeatherIcon.
func ExternalLink() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>external-link</title><path d="M18 13v6a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h6"></path><polyline points="15 3 21 3 21 9"></polyline><line x1="10" y1="14" x2="21" y2="3"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Eye returns the "eye" FeatherIcon.
func Eye() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>eye</title><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// EyeOff returns the "eye-off" FeatherIcon.
func EyeOff() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>eye-off</title><path d="M17.94 17.94A10.07 10.07 0 0 1 12 20c-7 0-11-8-11-8a18.45 18.45 0 0 1 5.06-5.94M9.9 4.24A9.12 9.12 0 0 1 12 4c7 0 11 8 11 8a18.5 18.5 0 0 1-2.16 3.19m-6.72-1.07a3 3 0 1 1-4.24-4.24"></path><line x1="1" y1="1" x2="23" y2="23"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Facebook returns the "facebook" FeatherIcon.
func Facebook() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>facebook</title><path d="M18 2h-3a5 5 0 0 0-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 0 1 1-1h3z"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// FastForward returns the "fast-forward" FeatherIcon.
func FastForward() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>fast-forward</title><polygon points="13 19 22 12 13 5 13 19"></polygon><polygon points="2 19 11 12 2 5 2 19"></polygon></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Feather returns the "feather" FeatherIcon.
func Feather() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>feather</title><path d="M20.24 12.24a6 6 0 0 0-8.49-8.49L5 10.5V19h8.5z"></path><line x1="16" y1="8" x2="2" y2="22"></line><line x1="17.5" y1="15" x2="9" y2="15"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Figma returns the "figma" FeatherIcon.
func Figma() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>figma</title><path d="M5 5.5A3.5 3.5 0 0 1 8.5 2H12v7H8.5A3.5 3.5 0 0 1 5 5.5z"></path><path d="M12 2h3.5a3.5 3.5 0 1 1 0 7H12V2z"></path><path d="M12 12.5a3.5 3.5 0 1 1 7 0 3.5 3.5 0 1 1-7 0z"></path><path d="M5 19.5A3.5 3.5 0 0 1 8.5 16H12v3.5a3.5 3.5 0 1 1-7 0z"></path><path d="M5 12.5A3.5 3.5 0 0 1 8.5 9H12v7H8.5A3.5 3.5 0 0 1 5 12.5z"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// File returns the "file" FeatherIcon.
func File() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>file</title><path d="M13 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V9z"></path><polyline points="13 2 13 9 20 9"></polyline></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// FileMinus returns the "file-minus" FeatherIcon.
func FileMinus() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>file-minus</title><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="9" y1="15" x2="15" y2="15"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// FilePlus returns the "file-plus" FeatherIcon.
func FilePlus() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>file-plus</title><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="12" y1="18" x2="12" y2="12"></line><line x1="9" y1="15" x2="15" y2="15"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// FileText returns the "file-text" FeatherIcon.
func FileText() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>file-text</title><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Film returns the "film" FeatherIcon.
func Film() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>film</title><rect x="2" y="2" width="20" height="20" rx="2.18" ry="2.18"></rect><line x1="7" y1="2" x2="7" y2="22"></line><line x1="17" y1="2" x2="17" y2="22"></line><line x1="2" y1="12" x2="22" y2="12"></line><line x1="2" y1="7" x2="7" y2="7"></line><line x1="2" y1="17" x2="7" y2="17"></line><line x1="17" y1="17" x2="22" y2="17"></line><line x1="17" y1="7" x2="22" y2="7"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Filter returns the "filter" FeatherIcon.
func Filter() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>filter</title><polygon points="22 3 2 3 10 12.46 10 19 14 21 14 12.46 22 3"></polygon></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Flag returns the "flag" FeatherIcon.
func Flag() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>flag</title><path d="M4 15s1-1 4-1 5 2 8 2 4-1 4-1V3s-1 1-4 1-5-2-8-2-4 1-4 1z"></path><line x1="4" y1="22" x2="4" y2="15"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Folder returns the "folder" FeatherIcon.
func Folder() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>folder</title><path d="M22 19a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h5l2 3h9a2 2 0 0 1 2 2z"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// FolderMinus returns the "folder-minus" FeatherIcon.
func FolderMinus() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>folder-minus</title><path d="M22 19a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h5l2 3h9a2 2 0 0 1 2 2z"></path><line x1="9" y1="14" x2="15" y2="14"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// FolderPlus returns the "folder-plus" FeatherIcon.
func FolderPlus() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>folder-plus</title><path d="M22 19a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h5l2 3h9a2 2 0 0 1 2 2z"></path><line x1="12" y1="11" x2="12" y2="17"></line><line x1="9" y1="14" x2="15" y2="14"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Framer returns the "framer" FeatherIcon.
func Framer() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>framer</title><path d="M5 16V9h14V2H5l14 14h-7m-7 0l7 7v-7m-7 0h7"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Frown returns the "frown" FeatherIcon.
func Frown() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>frown</title><circle cx="12" cy="12" r="10"></circle><path d="M16 16s-1.5-2-4-2-4 2-4 2"></path><line x1="9" y1="9" x2="9.01" y2="9"></line><line x1="15" y1="9" x2="15.01" y2="9"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Gift returns the "gift" FeatherIcon.
func Gift() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>gift</title><polyline points="20 12 20 22 4 22 4 12"></polyline><rect x="2" y="7" width="20" height="5"></rect><line x1="12" y1="22" x2="12" y2="7"></line><path d="M12 7H7.5a2.5 2.5 0 0 1 0-5C11 2 12 7 12 7z"></path><path d="M12 7h4.5a2.5 2.5 0 0 0 0-5C13 2 12 7 12 7z"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// GitBranch returns the "git-branch" FeatherIcon.
func GitBranch() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>git-branch</title><line x1="6" y1="3" x2="6" y2="15"></line><circle cx="18" cy="6" r="3"></circle><circle cx="6" cy="18" r="3"></circle><path d="M18 9a9 9 0 0 1-9 9"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// GitCommit returns the "git-commit" FeatherIcon.
func GitCommit() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>git-commit</title><circle cx="12" cy="12" r="4"></circle><line x1="1.05" y1="12" x2="7" y2="12"></line><line x1="17.01" y1="12" x2="22.96" y2="12"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// GitMerge returns the "git-merge" FeatherIcon.
func GitMerge() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>git-merge</title><circle cx="18" cy="18" r="3"></circle><circle cx="6" cy="6" r="3"></circle><path d="M6 21V9a9 9 0 0 0 9 9"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// GitPullRequest returns the "git-pull-request" FeatherIcon.
func GitPullRequest() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>git-pull-request</title><circle cx="18" cy="18" r="3"></circle><circle cx="6" cy="6" r="3"></circle><path d="M13 6h3a2 2 0 0 1 2 2v7"></path><line x1="6" y1="9" x2="6" y2="21"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Github returns the "github" FeatherIcon.
func Github() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>github</title><path d="M9 19c-5 1.5-5-2.5-7-3m14 6v-3.87a3.37 3.37 0 0 0-.94-2.61c3.14-.35 6.44-1.54 6.44-7A5.44 5.44 0 0 0 20 4.77 5.07 5.07 0 0 0 19.91 1S18.73.65 16 2.48a13.38 13.38 0 0 0-7 0C6.27.65 5.09 1 5.09 1A5.07 5.07 0 0 0 5 4.77a5.44 5.44 0 0 0-1.5 3.78c0 5.42 3.3 6.61 6.44 7A3.37 3.37 0 0 0 9 18.13V22"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Gitlab returns the "gitlab" FeatherIcon.
func Gitlab() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>gitlab</title><path d="M22.65 14.39L12 22.13 1.35 14.39a.84.84 0 0 1-.3-.94l1.22-3.78 2.44-7.51A.42.42 0 0 1 4.82 2a.43.43 0 0 1 .58 0 .42.42 0 0 1 .11.18l2.44 7.49h8.1l2.44-7.51A.42.42 0 0 1 18.6 2a.43.43 0 0 1 .58 0 .42.42 0 0 1 .11.18l2.44 7.51L23 13.45a.84.84 0 0 1-.35.94z"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Globe returns the "globe" FeatherIcon.
func Globe() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>globe</title><circle cx="12" cy="12" r="10"></circle><line x1="2" y1="12" x2="22" y2="12"></line><path d="M12 2a15.3 15.3 0 0 1 4 10 15.3 15.3 0 0 1-4 10 15.3 15.3 0 0 1-4-10 15.3 15.3 0 0 1 4-10z"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Grid returns the "grid" FeatherIcon.
func Grid() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>grid</title><rect x="3" y="3" width="7" height="7"></rect><rect x="14" y="3" width="7" height="7"></rect><rect x="14" y="14" width="7" height="7"></rect><rect x="3" y="14" width="7" height="7"></rect></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// HardDrive returns the "hard-drive" FeatherIcon.
func HardDrive() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>hard-drive</title><line x1="22" y1="12" x2="2" y2="12"></line><path d="M5.45 5.11L2 12v6a2 2 0 0 0 2 2h16a2 2 0 0 0 2-2v-6l-3.45-6.89A2 2 0 0 0 16.76 4H7.24a2 2 0 0 0-1.79 1.11z"></path><line x1="6" y1="16" x2="6.01" y2="16"></line><line x1="10" y1="16" x2="10.01" y2="16"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Hash returns the "hash" FeatherIcon.
func Hash() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>hash</title><line x1="4" y1="9" x2="20" y2="9"></line><line x1="4" y1="15" x2="20" y2="15"></line><line x1="10" y1="3" x2="8" y2="21"></line><line x1="16" y1="3" x2="14" y2="21"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Headphones returns the "headphones" FeatherIcon.
func Headphones() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>headphones</title><path d="M3 18v-6a9 9 0 0 1 18 0v6"></path><path d="M21 19a2 2 0 0 1-2 2h-1a2 2 0 0 1-2-2v-3a2 2 0 0 1 2-2h3zM3 19a2 2 0 0 0 2 2h1a2 2 0 0 0 2-2v-3a2 2 0 0 0-2-2H3z"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Heart returns the "heart" FeatherIcon.
func Heart() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>heart</title><path d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// HelpCircle returns the "help-circle" FeatherIcon.
func HelpCircle() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>help-circle</title><circle cx="12" cy="12" r="10"></circle><path d="M9.09 9a3 3 0 0 1 5.83 1c0 2-3 3-3 3"></path><line x1="12" y1="17" x2="12.01" y2="17"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Hexagon returns the "hexagon" FeatherIcon.
func Hexagon() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>hexagon</title><path d="M21 16V8a2 2 0 0 0-1-1.73l-7-4a2 2 0 0 0-2 0l-7 4A2 2 0 0 0 3 8v8a2 2 0 0 0 1 1.73l7 4a2 2 0 0 0 2 0l7-4A2 2 0 0 0 21 16z"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Home returns the "home" FeatherIcon.
func Home() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>home</title><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Image returns the "image" FeatherIcon.
func Image() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>image</title><rect x="3" y="3" width="18" height="18" rx="2" ry="2"></rect><circle cx="8.5" cy="8.5" r="1.5"></circle><polyline points="21 15 16 10 5 21"></polyline></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Inbox returns the "inbox" FeatherIcon.
func Inbox() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>inbox</title><polyline points="22 12 16 12 14 15 10 15 8 12 2 12"></polyline><path d="M5.45 5.11L2 12v6a2 2 0 0 0 2 2h16a2 2 0 0 0 2-2v-6l-3.45-6.89A2 2 0 0 0 16.76 4H7.24a2 2 0 0 0-1.79 1.11z"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Info returns the "info" FeatherIcon.
func Info() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>info</title><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="16" x2="12" y2="12"></line><line x1="12" y1="8" x2="12.01" y2="8"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Instagram returns the "instagram" FeatherIcon.
func Instagram() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>instagram</title><rect x="2" y="2" width="20" height="20" rx="5" ry="5"></rect><path d="M16 11.37A4 4 0 1 1 12.63 8 4 4 0 0 1 16 11.37z"></path><line x1="17.5" y1="6.5" x2="17.51" y2="6.5"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Italic returns the "italic" FeatherIcon.
func Italic() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>italic</title><line x1="19" y1="4" x2="10" y2="4"></line><line x1="14" y1="20" x2="5" y2="20"></line><line x1="15" y1="4" x2="9" y2="20"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Key returns the "key" FeatherIcon.
func Key() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>key</title><path d="M21 2l-2 2m-7.61 7.61a5.5 5.5 0 1 1-7.778 7.778 5.5 5.5 0 0 1 7.777-7.777zm0 0L15.5 7.5m0 0l3 3L22 7l-3-3m-3.5 3.5L19 4"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Layers returns the "layers" FeatherIcon.
func Layers() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>layers</title><polygon points="12 2 2 7 12 12 22 7 12 2"></polygon><polyline points="2 17 12 22 22 17"></polyline><polyline points="2 12 12 17 22 12"></polyline></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Layout returns the "layout" FeatherIcon.
func Layout() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>layout</title><rect x="3" y="3" width="18" height="18" rx="2" ry="2"></rect><line x1="3" y1="9" x2="21" y2="9"></line><line x1="9" y1="21" x2="9" y2="9"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// LifeBuoy returns the "life-buoy" FeatherIcon.
func LifeBuoy() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>life-buoy</title><circle cx="12" cy="12" r="10"></circle><circle cx="12" cy="12" r="4"></circle><line x1="4.93" y1="4.93" x2="9.17" y2="9.17"></line><line x1="14.83" y1="14.83" x2="19.07" y2="19.07"></line><line x1="14.83" y1="9.17" x2="19.07" y2="4.93"></line><line x1="14.83" y1="9.17" x2="18.36" y2="5.64"></line><line x1="4.93" y1="19.07" x2="9.17" y2="14.83"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Link returns the "link" FeatherIcon.
func Link() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>link</title><path d="M10 13a5 5 0 0 0 7.54.54l3-3a5 5 0 0 0-7.07-7.07l-1.72 1.71"></path><path d="M14 11a5 5 0 0 0-7.54-.54l-3 3a5 5 0 0 0 7.07 7.07l1.71-1.71"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Link2 returns the "link-2" FeatherIcon.
func Link2() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>link-2</title><path d="M15 7h3a5 5 0 0 1 5 5 5 5 0 0 1-5 5h-3m-6 0H6a5 5 0 0 1-5-5 5 5 0 0 1 5-5h3"></path><line x1="8" y1="12" x2="16" y2="12"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Linkedin returns the "linkedin" FeatherIcon.
func Linkedin() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>linkedin</title><path d="M16 8a6 6 0 0 1 6 6v7h-4v-7a2 2 0 0 0-2-2 2 2 0 0 0-2 2v7h-4v-7a6 6 0 0 1 6-6z"></path><rect x="2" y="9" width="4" height="12"></rect><circle cx="4" cy="4" r="2"></circle></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// List returns the "list" FeatherIcon.
func List() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>list</title><line x1="8" y1="6" x2="21" y2="6"></line><line x1="8" y1="12" x2="21" y2="12"></line><line x1="8" y1="18" x2="21" y2="18"></line><line x1="3" y1="6" x2="3.01" y2="6"></line><line x1="3" y1="12" x2="3.01" y2="12"></line><line x1="3" y1="18" x2="3.01" y2="18"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Loader returns the "loader" FeatherIcon.
func Loader() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>loader</title><line x1="12" y1="2" x2="12" y2="6"></line><line x1="12" y1="18" x2="12" y2="22"></line><line x1="4.93" y1="4.93" x2="7.76" y2="7.76"></line><line x1="16.24" y1="16.24" x2="19.07" y2="19.07"></line><line x1="2" y1="12" x2="6" y2="12"></line><line x1="18" y1="12" x2="22" y2="12"></line><line x1="4.93" y1="19.07" x2="7.76" y2="16.24"></line><line x1="16.24" y1="7.76" x2="19.07" y2="4.93"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Lock returns the "lock" FeatherIcon.
func Lock() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>lock</title><rect x="3" y="11" width="18" height="11" rx="2" ry="2"></rect><path d="M7 11V7a5 5 0 0 1 10 0v4"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// LogIn returns the "log-in" FeatherIcon.
func LogIn() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>log-in</title><path d="M15 3h4a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2h-4"></path><polyline points="10 17 15 12 10 7"></polyline><line x1="15" y1="12" x2="3" y2="12"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// LogOut returns the "log-out" FeatherIcon.
func LogOut() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>log-out</title><path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4"></path><polyline points="16 17 21 12 16 7"></polyline><line x1="21" y1="12" x2="9" y2="12"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Mail returns the "mail" FeatherIcon.
func Mail() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>mail</title><path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path><polyline points="22,6 12,13 2,6"></polyline></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Map returns the "map" FeatherIcon.
func Map() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>map</title><polygon points="1 6 1 22 8 18 16 22 23 18 23 2 16 6 8 2 1 6"></polygon><line x1="8" y1="2" x2="8" y2="18"></line><line x1="16" y1="6" x2="16" y2="22"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// MapPin returns the "map-pin" FeatherIcon.
func MapPin() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>map-pin</title><path d="M21 10c0 7-9 13-9 13s-9-6-9-13a9 9 0 0 1 18 0z"></path><circle cx="12" cy="10" r="3"></circle></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Maximize returns the "maximize" FeatherIcon.
func Maximize() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>maximize</title><path d="M8 3H5a2 2 0 0 0-2 2v3m18 0V5a2 2 0 0 0-2-2h-3m0 18h3a2 2 0 0 0 2-2v-3M3 16v3a2 2 0 0 0 2 2h3"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Maximize2 returns the "maximize-2" FeatherIcon.
func Maximize2() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>maximize-2</title><polyline points="15 3 21 3 21 9"></polyline><polyline points="9 21 3 21 3 15"></polyline><line x1="21" y1="3" x2="14" y2="10"></line><line x1="3" y1="21" x2="10" y2="14"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Meh returns the "meh" FeatherIcon.
func Meh() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>meh</title><circle cx="12" cy="12" r="10"></circle><line x1="8" y1="15" x2="16" y2="15"></line><line x1="9" y1="9" x2="9.01" y2="9"></line><line x1="15" y1="9" x2="15.01" y2="9"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Menu returns the "menu" FeatherIcon.
func Menu() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>menu</title><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// MessageCircle returns the "message-circle" FeatherIcon.
func MessageCircle() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>message-circle</title><path d="M21 11.5a8.38 8.38 0 0 1-.9 3.8 8.5 8.5 0 0 1-7.6 4.7 8.38 8.38 0 0 1-3.8-.9L3 21l1.9-5.7a8.38 8.38 0 0 1-.9-3.8 8.5 8.5 0 0 1 4.7-7.6 8.38 8.38 0 0 1 3.8-.9h.5a8.48 8.48 0 0 1 8 8v.5z"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// MessageSquare returns the "message-square" FeatherIcon.
func MessageSquare() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>message-square</title><path d="M21 15a2 2 0 0 1-2 2H7l-4 4V5a2 2 0 0 1 2-2h14a2 2 0 0 1 2 2z"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Mic returns the "mic" FeatherIcon.
func Mic() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>mic</title><path d="M12 1a3 3 0 0 0-3 3v8a3 3 0 0 0 6 0V4a3 3 0 0 0-3-3z"></path><path d="M19 10v2a7 7 0 0 1-14 0v-2"></path><line x1="12" y1="19" x2="12" y2="23"></line><line x1="8" y1="23" x2="16" y2="23"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// MicOff returns the "mic-off" FeatherIcon.
func MicOff() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>mic-off</title><line x1="1" y1="1" x2="23" y2="23"></line><path d="M9 9v3a3 3 0 0 0 5.12 2.12M15 9.34V4a3 3 0 0 0-5.94-.6"></path><path d="M17 16.95A7 7 0 0 1 5 12v-2m14 0v2a7 7 0 0 1-.11 1.23"></path><line x1="12" y1="19" x2="12" y2="23"></line><line x1="8" y1="23" x2="16" y2="23"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Minimize returns the "minimize" FeatherIcon.
func Minimize() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>minimize</title><path d="M8 3v3a2 2 0 0 1-2 2H3m18 0h-3a2 2 0 0 1-2-2V3m0 18v-3a2 2 0 0 1 2-2h3M3 16h3a2 2 0 0 1 2 2v3"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Minimize2 returns the "minimize-2" FeatherIcon.
func Minimize2() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>minimize-2</title><polyline points="4 14 10 14 10 20"></polyline><polyline points="20 10 14 10 14 4"></polyline><line x1="14" y1="10" x2="21" y2="3"></line><line x1="3" y1="21" x2="10" y2="14"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Minus returns the "minus" FeatherIcon.
func Minus() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>minus</title><line x1="5" y1="12" x2="19" y2="12"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// MinusCircle returns the "minus-circle" FeatherIcon.
func MinusCircle() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>minus-circle</title><circle cx="12" cy="12" r="10"></circle><line x1="8" y1="12" x2="16" y2="12"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// MinusSquare returns the "minus-square" FeatherIcon.
func MinusSquare() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>minus-square</title><rect x="3" y="3" width="18" height="18" rx="2" ry="2"></rect><line x1="8" y1="12" x2="16" y2="12"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Monitor returns the "monitor" FeatherIcon.
func Monitor() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>monitor</title><rect x="2" y="3" width="20" height="14" rx="2" ry="2"></rect><line x1="8" y1="21" x2="16" y2="21"></line><line x1="12" y1="17" x2="12" y2="21"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Moon returns the "moon" FeatherIcon.
func Moon() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>moon</title><path d="M21 12.79A9 9 0 1 1 11.21 3 7 7 0 0 0 21 12.79z"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// MoreHorizontal returns the "more-horizontal" FeatherIcon.
func MoreHorizontal() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>more-horizontal</title><circle cx="12" cy="12" r="1"></circle><circle cx="19" cy="12" r="1"></circle><circle cx="5" cy="12" r="1"></circle></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// MoreVertical returns the "more-vertical" FeatherIcon.
func MoreVertical() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>more-vertical</title><circle cx="12" cy="12" r="1"></circle><circle cx="12" cy="5" r="1"></circle><circle cx="12" cy="19" r="1"></circle></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// MousePointer returns the "mouse-pointer" FeatherIcon.
func MousePointer() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>mouse-pointer</title><path d="M3 3l7.07 16.97 2.51-7.39 7.39-2.51L3 3z"></path><path d="M13 13l6 6"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Move returns the "move" FeatherIcon.
func Move() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>move</title><polyline points="5 9 2 12 5 15"></polyline><polyline points="9 5 12 2 15 5"></polyline><polyline points="15 19 12 22 9 19"></polyline><polyline points="19 9 22 12 19 15"></polyline><line x1="2" y1="12" x2="22" y2="12"></line><line x1="12" y1="2" x2="12" y2="22"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Music returns the "music" FeatherIcon.
func Music() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>music</title><path d="M9 18V5l12-2v13"></path><circle cx="6" cy="18" r="3"></circle><circle cx="18" cy="16" r="3"></circle></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Navigation returns the "navigation" FeatherIcon.
func Navigation() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>navigation</title><polygon points="3 11 22 2 13 21 11 13 3 11"></polygon></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Navigation2 returns the "navigation-2" FeatherIcon.
func Navigation2() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>navigation-2</title><polygon points="12 2 19 21 12 17 5 21 12 2"></polygon></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Octagon returns the "octagon" FeatherIcon.
func Octagon() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>octagon</title><polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Package returns the "package" FeatherIcon.
func Package() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>package</title><line x1="16.5" y1="9.4" x2="7.5" y2="4.21"></line><path d="M21 16V8a2 2 0 0 0-1-1.73l-7-4a2 2 0 0 0-2 0l-7 4A2 2 0 0 0 3 8v8a2 2 0 0 0 1 1.73l7 4a2 2 0 0 0 2 0l7-4A2 2 0 0 0 21 16z"></path><polyline points="3.27 6.96 12 12.01 20.73 6.96"></polyline><line x1="12" y1="22.08" x2="12" y2="12"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Paperclip returns the "paperclip" FeatherIcon.
func Paperclip() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>paperclip</title><path d="M21.44 11.05l-9.19 9.19a6 6 0 0 1-8.49-8.49l9.19-9.19a4 4 0 0 1 5.66 5.66l-9.2 9.19a2 2 0 0 1-2.83-2.83l8.49-8.48"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Pause returns the "pause" FeatherIcon.
func Pause() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>pause</title><rect x="6" y="4" width="4" height="16"></rect><rect x="14" y="4" width="4" height="16"></rect></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// PauseCircle returns the "pause-circle" FeatherIcon.
func PauseCircle() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>pause-circle</title><circle cx="12" cy="12" r="10"></circle><line x1="10" y1="15" x2="10" y2="9"></line><line x1="14" y1="15" x2="14" y2="9"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// PenTool returns the "pen-tool" FeatherIcon.
func PenTool() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>pen-tool</title><path d="M12 19l7-7 3 3-7 7-3-3z"></path><path d="M18 13l-1.5-7.5L2 2l3.5 14.5L13 18l5-5z"></path><path d="M2 2l7.586 7.586"></path><circle cx="11" cy="11" r="2"></circle></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Percent returns the "percent" FeatherIcon.
func Percent() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>percent</title><line x1="19" y1="5" x2="5" y2="19"></line><circle cx="6.5" cy="6.5" r="2.5"></circle><circle cx="17.5" cy="17.5" r="2.5"></circle></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Phone returns the "phone" FeatherIcon.
func Phone() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>phone</title><path d="M22 16.92v3a2 2 0 0 1-2.18 2 19.79 19.79 0 0 1-8.63-3.07 19.5 19.5 0 0 1-6-6 19.79 19.79 0 0 1-3.07-8.67A2 2 0 0 1 4.11 2h3a2 2 0 0 1 2 1.72 12.84 12.84 0 0 0 .7 2.81 2 2 0 0 1-.45 2.11L8.09 9.91a16 16 0 0 0 6 6l1.27-1.27a2 2 0 0 1 2.11-.45 12.84 12.84 0 0 0 2.81.7A2 2 0 0 1 22 16.92z"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// PhoneCall returns the "phone-call" FeatherIcon.
func PhoneCall() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>phone-call</title><path d="M15.05 5A5 5 0 0 1 19 8.95M15.05 1A9 9 0 0 1 23 8.94m-1 7.98v3a2 2 0 0 1-2.18 2 19.79 19.79 0 0 1-8.63-3.07 19.5 19.5 0 0 1-6-6 19.79 19.79 0 0 1-3.07-8.67A2 2 0 0 1 4.11 2h3a2 2 0 0 1 2 1.72 12.84 12.84 0 0 0 .7 2.81 2 2 0 0 1-.45 2.11L8.09 9.91a16 16 0 0 0 6 6l1.27-1.27a2 2 0 0 1 2.11-.45 12.84 12.84 0 0 0 2.81.7A2 2 0 0 1 22 16.92z"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// PhoneForwarded returns the "phone-forwarded" FeatherIcon.
func PhoneForwarded() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>phone-forwarded</title><polyline points="19 1 23 5 19 9"></polyline><line x1="15" y1="5" x2="23" y2="5"></line><path d="M22 16.92v3a2 2 0 0 1-2.18 2 19.79 19.79 0 0 1-8.63-3.07 19.5 19.5 0 0 1-6-6 19.79 19.79 0 0 1-3.07-8.67A2 2 0 0 1 4.11 2h3a2 2 0 0 1 2 1.72 12.84 12.84 0 0 0 .7 2.81 2 2 0 0 1-.45 2.11L8.09 9.91a16 16 0 0 0 6 6l1.27-1.27a2 2 0 0 1 2.11-.45 12.84 12.84 0 0 0 2.81.7A2 2 0 0 1 22 16.92z"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// PhoneIncoming returns the "phone-incoming" FeatherIcon.
func PhoneIncoming() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>phone-incoming</title><polyline points="16 2 16 8 22 8"></polyline><line x1="23" y1="1" x2="16" y2="8"></line><path d="M22 16.92v3a2 2 0 0 1-2.18 2 19.79 19.79 0 0 1-8.63-3.07 19.5 19.5 0 0 1-6-6 19.79 19.79 0 0 1-3.07-8.67A2 2 0 0 1 4.11 2h3a2 2 0 0 1 2 1.72 12.84 12.84 0 0 0 .7 2.81 2 2 0 0 1-.45 2.11L8.09 9.91a16 16 0 0 0 6 6l1.27-1.27a2 2 0 0 1 2.11-.45 12.84 12.84 0 0 0 2.81.7A2 2 0 0 1 22 16.92z"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// PhoneMissed returns the "phone-missed" FeatherIcon.
func PhoneMissed() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>phone-missed</title><line x1="23" y1="1" x2="17" y2="7"></line><line x1="17" y1="1" x2="23" y2="7"></line><path d="M22 16.92v3a2 2 0 0 1-2.18 2 19.79 19.79 0 0 1-8.63-3.07 19.5 19.5 0 0 1-6-6 19.79 19.79 0 0 1-3.07-8.67A2 2 0 0 1 4.11 2h3a2 2 0 0 1 2 1.72 12.84 12.84 0 0 0 .7 2.81 2 2 0 0 1-.45 2.11L8.09 9.91a16 16 0 0 0 6 6l1.27-1.27a2 2 0 0 1 2.11-.45 12.84 12.84 0 0 0 2.81.7A2 2 0 0 1 22 16.92z"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// PhoneOff returns the "phone-off" FeatherIcon.
func PhoneOff() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>phone-off</title><path d="M10.68 13.31a16 16 0 0 0 3.41 2.6l1.27-1.27a2 2 0 0 1 2.11-.45 12.84 12.84 0 0 0 2.81.7 2 2 0 0 1 1.72 2v3a2 2 0 0 1-2.18 2 19.79 19.79 0 0 1-8.63-3.07 19.42 19.42 0 0 1-3.33-2.67m-2.67-3.34a19.79 19.79 0 0 1-3.07-8.63A2 2 0 0 1 4.11 2h3a2 2 0 0 1 2 1.72 12.84 12.84 0 0 0 .7 2.81 2 2 0 0 1-.45 2.11L8.09 9.91"></path><line x1="23" y1="1" x2="1" y2="23"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// PhoneOutgoing returns the "phone-outgoing" FeatherIcon.
func PhoneOutgoing() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>phone-outgoing</title><polyline points="23 7 23 1 17 1"></polyline><line x1="16" y1="8" x2="23" y2="1"></line><path d="M22 16.92v3a2 2 0 0 1-2.18 2 19.79 19.79 0 0 1-8.63-3.07 19.5 19.5 0 0 1-6-6 19.79 19.79 0 0 1-3.07-8.67A2 2 0 0 1 4.11 2h3a2 2 0 0 1 2 1.72 12.84 12.84 0 0 0 .7 2.81 2 2 0 0 1-.45 2.11L8.09 9.91a16 16 0 0 0 6 6l1.27-1.27a2 2 0 0 1 2.11-.45 12.84 12.84 0 0 0 2.81.7A2 2 0 0 1 22 16.92z"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// PieChart returns the "pie-chart" FeatherIcon.
func PieChart() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>pie-chart</title><path d="M21.21 15.89A10 10 0 1 1 8 2.83"></path><path d="M22 12A10 10 0 0 0 12 2v10z"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Play returns the "play" FeatherIcon.
func Play() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>play</title><polygon points="5 3 19 12 5 21 5 3"></polygon></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// PlayCircle returns the "play-circle" FeatherIcon.
func PlayCircle() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>play-circle</title><circle cx="12" cy="12" r="10"></circle><polygon points="10 8 16 12 10 16 10 8"></polygon></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Plus returns the "plus" FeatherIcon.
func Plus() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>plus</title><line x1="12" y1="5" x2="12" y2="19"></line><line x1="5" y1="12" x2="19" y2="12"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// PlusCircle returns the "plus-circle" FeatherIcon.
func PlusCircle() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>plus-circle</title><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="8" x2="12" y2="16"></line><line x1="8" y1="12" x2="16" y2="12"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// PlusSquare returns the "plus-square" FeatherIcon.
func PlusSquare() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>plus-square</title><rect x="3" y="3" width="18" height="18" rx="2" ry="2"></rect><line x1="12" y1="8" x2="12" y2="16"></line><line x1="8" y1="12" x2="16" y2="12"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Pocket returns the "pocket" FeatherIcon.
func Pocket() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>pocket</title><path d="M4 3h16a2 2 0 0 1 2 2v6a10 10 0 0 1-10 10A10 10 0 0 1 2 11V5a2 2 0 0 1 2-2z"></path><polyline points="8 10 12 14 16 10"></polyline></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Power returns the "power" FeatherIcon.
func Power() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>power</title><path d="M18.36 6.64a9 9 0 1 1-12.73 0"></path><line x1="12" y1="2" x2="12" y2="12"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Printer returns the "printer" FeatherIcon.
func Printer() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>printer</title><polyline points="6 9 6 2 18 2 18 9"></polyline><path d="M6 18H4a2 2 0 0 1-2-2v-5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v5a2 2 0 0 1-2 2h-2"></path><rect x="6" y="14" width="12" height="8"></rect></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Radio returns the "radio" FeatherIcon.
func Radio() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>radio</title><circle cx="12" cy="12" r="2"></circle><path d="M16.24 7.76a6 6 0 0 1 0 8.49m-8.48-.01a6 6 0 0 1 0-8.49m11.31-2.82a10 10 0 0 1 0 14.14m-14.14 0a10 10 0 0 1 0-14.14"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// RefreshCcw returns the "refresh-ccw" FeatherIcon.
func RefreshCcw() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>refresh-ccw</title><polyline points="1 4 1 10 7 10"></polyline><polyline points="23 20 23 14 17 14"></polyline><path d="M20.49 9A9 9 0 0 0 5.64 5.64L1 10m22 4l-4.64 4.36A9 9 0 0 1 3.51 15"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// RefreshCw returns the "refresh-cw" FeatherIcon.
func RefreshCw() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>refresh-cw</title><polyline points="23 4 23 10 17 10"></polyline><polyline points="1 20 1 14 7 14"></polyline><path d="M3.51 9a9 9 0 0 1 14.85-3.36L23 10M1 14l4.64 4.36A9 9 0 0 0 20.49 15"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Repeat returns the "repeat" FeatherIcon.
func Repeat() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>repeat</title><polyline points="17 1 21 5 17 9"></polyline><path d="M3 11V9a4 4 0 0 1 4-4h14"></path><polyline points="7 23 3 19 7 15"></polyline><path d="M21 13v2a4 4 0 0 1-4 4H3"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Rewind returns the "rewind" FeatherIcon.
func Rewind() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>rewind</title><polygon points="11 19 2 12 11 5 11 19"></polygon><polygon points="22 19 13 12 22 5 22 19"></polygon></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// RotateCcw returns the "rotate-ccw" FeatherIcon.
func RotateCcw() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>rotate-ccw</title><polyline points="1 4 1 10 7 10"></polyline><path d="M3.51 15a9 9 0 1 0 2.13-9.36L1 10"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// RotateCw returns the "rotate-cw" FeatherIcon.
func RotateCw() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>rotate-cw</title><polyline points="23 4 23 10 17 10"></polyline><path d="M20.49 15a9 9 0 1 1-2.12-9.36L23 10"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Rss returns the "rss" FeatherIcon.
func Rss() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>rss</title><path d="M4 11a9 9 0 0 1 9 9"></path><path d="M4 4a16 16 0 0 1 16 16"></path><circle cx="5" cy="19" r="1"></circle></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Save returns the "save" FeatherIcon.
func Save() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>save</title><path d="M19 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11l5 5v11a2 2 0 0 1-2 2z"></path><polyline points="17 21 17 13 7 13 7 21"></polyline><polyline points="7 3 7 8 15 8"></polyline></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Scissors returns the "scissors" FeatherIcon.
func Scissors() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>scissors</title><circle cx="6" cy="6" r="3"></circle><circle cx="6" cy="18" r="3"></circle><line x1="20" y1="4" x2="8.12" y2="15.88"></line><line x1="14.47" y1="14.48" x2="20" y2="20"></line><line x1="8.12" y1="8.12" x2="12" y2="12"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Search returns the "search" FeatherIcon.
func Search() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>search</title><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Send returns the "send" FeatherIcon.
func Send() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>send</title><line x1="22" y1="2" x2="11" y2="13"></line><polygon points="22 2 15 22 11 13 2 9 22 2"></polygon></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Server returns the "server" FeatherIcon.
func Server() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>server</title><rect x="2" y="2" width="20" height="8" rx="2" ry="2"></rect><rect x="2" y="14" width="20" height="8" rx="2" ry="2"></rect><line x1="6" y1="6" x2="6.01" y2="6"></line><line x1="6" y1="18" x2="6.01" y2="18"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Settings returns the "settings" FeatherIcon.
func Settings() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>settings</title><circle cx="12" cy="12" r="3"></circle><path d="M19.4 15a1.65 1.65 0 0 0 .33 1.82l.06.06a2 2 0 0 1 0 2.83 2 2 0 0 1-2.83 0l-.06-.06a1.65 1.65 0 0 0-1.82-.33 1.65 1.65 0 0 0-1 1.51V21a2 2 0 0 1-2 2 2 2 0 0 1-2-2v-.09A1.65 1.65 0 0 0 9 19.4a1.65 1.65 0 0 0-1.82.33l-.06.06a2 2 0 0 1-2.83 0 2 2 0 0 1 0-2.83l.06-.06a1.65 1.65 0 0 0 .33-1.82 1.65 1.65 0 0 0-1.51-1H3a2 2 0 0 1-2-2 2 2 0 0 1 2-2h.09A1.65 1.65 0 0 0 4.6 9a1.65 1.65 0 0 0-.33-1.82l-.06-.06a2 2 0 0 1 0-2.83 2 2 0 0 1 2.83 0l.06.06a1.65 1.65 0 0 0 1.82.33H9a1.65 1.65 0 0 0 1-1.51V3a2 2 0 0 1 2-2 2 2 0 0 1 2 2v.09a1.65 1.65 0 0 0 1 1.51 1.65 1.65 0 0 0 1.82-.33l.06-.06a2 2 0 0 1 2.83 0 2 2 0 0 1 0 2.83l-.06.06a1.65 1.65 0 0 0-.33 1.82V9a1.65 1.65 0 0 0 1.51 1H21a2 2 0 0 1 2 2 2 2 0 0 1-2 2h-.09a1.65 1.65 0 0 0-1.51 1z"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Share returns the "share" FeatherIcon.
func Share() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>share</title><path d="M4 12v8a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2v-8"></path><polyline points="16 6 12 2 8 6"></polyline><line x1="12" y1="2" x2="12" y2="15"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Share2 returns the "share-2" FeatherIcon.
func Share2() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>share-2</title><circle cx="18" cy="5" r="3"></circle><circle cx="6" cy="12" r="3"></circle><circle cx="18" cy="19" r="3"></circle><line x1="8.59" y1="13.51" x2="15.42" y2="17.49"></line><line x1="15.41" y1="6.51" x2="8.59" y2="10.49"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Shield returns the "shield" FeatherIcon.
func Shield() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>shield</title><path d="M12 22s8-4 8-10V5l-8-3-8 3v7c0 6 8 10 8 10z"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// ShieldOff returns the "shield-off" FeatherIcon.
func ShieldOff() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>shield-off</title><path d="M19.69 14a6.9 6.9 0 0 0 .31-2V5l-8-3-3.16 1.18"></path><path d="M4.73 4.73L4 5v7c0 6 8 10 8 10a20.29 20.29 0 0 0 5.62-4.38"></path><line x1="1" y1="1" x2="23" y2="23"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// ShoppingBag returns the "shopping-bag" FeatherIcon.
func ShoppingBag() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>shopping-bag</title><path d="M6 2L3 6v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2V6l-3-4z"></path><line x1="3" y1="6" x2="21" y2="6"></line><path d="M16 10a4 4 0 0 1-8 0"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// ShoppingCart returns the "shopping-cart" FeatherIcon.
func ShoppingCart() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>shopping-cart</title><circle cx="9" cy="21" r="1"></circle><circle cx="20" cy="21" r="1"></circle><path d="M1 1h4l2.68 13.39a2 2 0 0 0 2 1.61h9.72a2 2 0 0 0 2-1.61L23 6H6"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Shuffle returns the "shuffle" FeatherIcon.
func Shuffle() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>shuffle</title><polyline points="16 3 21 3 21 8"></polyline><line x1="4" y1="20" x2="21" y2="3"></line><polyline points="21 16 21 21 16 21"></polyline><line x1="15" y1="15" x2="21" y2="21"></line><line x1="4" y1="4" x2="9" y2="9"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Sidebar returns the "sidebar" FeatherIcon.
func Sidebar() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>sidebar</title><rect x="3" y="3" width="18" height="18" rx="2" ry="2"></rect><line x1="9" y1="3" x2="9" y2="21"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// SkipBack returns the "skip-back" FeatherIcon.
func SkipBack() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>skip-back</title><polygon points="19 20 9 12 19 4 19 20"></polygon><line x1="5" y1="19" x2="5" y2="5"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// SkipForward returns the "skip-forward" FeatherIcon.
func SkipForward() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>skip-forward</title><polygon points="5 4 15 12 5 20 5 4"></polygon><line x1="19" y1="5" x2="19" y2="19"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Slack returns the "slack" FeatherIcon.
func Slack() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>slack</title><path d="M14.5 10c-.83 0-1.5-.67-1.5-1.5v-5c0-.83.67-1.5 1.5-1.5s1.5.67 1.5 1.5v5c0 .83-.67 1.5-1.5 1.5z"></path><path d="M20.5 10H19V8.5c0-.83.67-1.5 1.5-1.5s1.5.67 1.5 1.5-.67 1.5-1.5 1.5z"></path><path d="M9.5 14c.83 0 1.5.67 1.5 1.5v5c0 .83-.67 1.5-1.5 1.5S8 21.33 8 20.5v-5c0-.83.67-1.5 1.5-1.5z"></path><path d="M3.5 14H5v1.5c0 .83-.67 1.5-1.5 1.5S2 16.33 2 15.5 2.67 14 3.5 14z"></path><path d="M14 14.5c0-.83.67-1.5 1.5-1.5h5c.83 0 1.5.67 1.5 1.5s-.67 1.5-1.5 1.5h-5c-.83 0-1.5-.67-1.5-1.5z"></path><path d="M15.5 19H14v1.5c0 .83.67 1.5 1.5 1.5s1.5-.67 1.5-1.5-.67-1.5-1.5-1.5z"></path><path d="M10 9.5C10 8.67 9.33 8 8.5 8h-5C2.67 8 2 8.67 2 9.5S2.67 11 3.5 11h5c.83 0 1.5-.67 1.5-1.5z"></path><path d="M8.5 5H10V3.5C10 2.67 9.33 2 8.5 2S7 2.67 7 3.5 7.67 5 8.5 5z"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Slash returns the "slash" FeatherIcon.
func Slash() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>slash</title><circle cx="12" cy="12" r="10"></circle><line x1="4.93" y1="4.93" x2="19.07" y2="19.07"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Sliders returns the "sliders" FeatherIcon.
func Sliders() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>sliders</title><line x1="4" y1="21" x2="4" y2="14"></line><line x1="4" y1="10" x2="4" y2="3"></line><line x1="12" y1="21" x2="12" y2="12"></line><line x1="12" y1="8" x2="12" y2="3"></line><line x1="20" y1="21" x2="20" y2="16"></line><line x1="20" y1="12" x2="20" y2="3"></line><line x1="1" y1="14" x2="7" y2="14"></line><line x1="9" y1="8" x2="15" y2="8"></line><line x1="17" y1="16" x2="23" y2="16"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Smartphone returns the "smartphone" FeatherIcon.
func Smartphone() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>smartphone</title><rect x="5" y="2" width="14" height="20" rx="2" ry="2"></rect><line x1="12" y1="18" x2="12.01" y2="18"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Smile returns the "smile" FeatherIcon.
func Smile() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>smile</title><circle cx="12" cy="12" r="10"></circle><path d="M8 14s1.5 2 4 2 4-2 4-2"></path><line x1="9" y1="9" x2="9.01" y2="9"></line><line x1="15" y1="9" x2="15.01" y2="9"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Speaker returns the "speaker" FeatherIcon.
func Speaker() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>speaker</title><rect x="4" y="2" width="16" height="20" rx="2" ry="2"></rect><circle cx="12" cy="14" r="4"></circle><line x1="12" y1="6" x2="12.01" y2="6"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Square returns the "square" FeatherIcon.
func Square() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>square</title><rect x="3" y="3" width="18" height="18" rx="2" ry="2"></rect></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Star returns the "star" FeatherIcon.
func Star() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>star</title><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// StopCircle returns the "stop-circle" FeatherIcon.
func StopCircle() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>stop-circle</title><circle cx="12" cy="12" r="10"></circle><rect x="9" y="9" width="6" height="6"></rect></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Sun returns the "sun" FeatherIcon.
func Sun() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>sun</title><circle cx="12" cy="12" r="5"></circle><line x1="12" y1="1" x2="12" y2="3"></line><line x1="12" y1="21" x2="12" y2="23"></line><line x1="4.22" y1="4.22" x2="5.64" y2="5.64"></line><line x1="18.36" y1="18.36" x2="19.78" y2="19.78"></line><line x1="1" y1="12" x2="3" y2="12"></line><line x1="21" y1="12" x2="23" y2="12"></line><line x1="4.22" y1="19.78" x2="5.64" y2="18.36"></line><line x1="18.36" y1="5.64" x2="19.78" y2="4.22"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Sunrise returns the "sunrise" FeatherIcon.
func Sunrise() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>sunrise</title><path d="M17 18a5 5 0 0 0-10 0"></path><line x1="12" y1="2" x2="12" y2="9"></line><line x1="4.22" y1="10.22" x2="5.64" y2="11.64"></line><line x1="1" y1="18" x2="3" y2="18"></line><line x1="21" y1="18" x2="23" y2="18"></line><line x1="18.36" y1="11.64" x2="19.78" y2="10.22"></line><line x1="23" y1="22" x2="1" y2="22"></line><polyline points="8 6 12 2 16 6"></polyline></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Sunset returns the "sunset" FeatherIcon.
func Sunset() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>sunset</title><path d="M17 18a5 5 0 0 0-10 0"></path><line x1="12" y1="9" x2="12" y2="2"></line><line x1="4.22" y1="10.22" x2="5.64" y2="11.64"></line><line x1="1" y1="18" x2="3" y2="18"></line><line x1="21" y1="18" x2="23" y2="18"></line><line x1="18.36" y1="11.64" x2="19.78" y2="10.22"></line><line x1="23" y1="22" x2="1" y2="22"></line><polyline points="16 5 12 9 8 5"></polyline></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Tablet returns the "tablet" FeatherIcon.
func Tablet() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>tablet</title><rect x="4" y="2" width="16" height="20" rx="2" ry="2"></rect><line x1="12" y1="18" x2="12.01" y2="18"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Tag returns the "tag" FeatherIcon.
func Tag() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>tag</title><path d="M20.59 13.41l-7.17 7.17a2 2 0 0 1-2.83 0L2 12V2h10l8.59 8.59a2 2 0 0 1 0 2.82z"></path><line x1="7" y1="7" x2="7.01" y2="7"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Target returns the "target" FeatherIcon.
func Target() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>target</title><circle cx="12" cy="12" r="10"></circle><circle cx="12" cy="12" r="6"></circle><circle cx="12" cy="12" r="2"></circle></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Terminal returns the "terminal" FeatherIcon.
func Terminal() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>terminal</title><polyline points="4 17 10 11 4 5"></polyline><line x1="12" y1="19" x2="20" y2="19"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Thermometer returns the "thermometer" FeatherIcon.
func Thermometer() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>thermometer</title><path d="M14 14.76V3.5a2.5 2.5 0 0 0-5 0v11.26a4.5 4.5 0 1 0 5 0z"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// ThumbsDown returns the "thumbs-down" FeatherIcon.
func ThumbsDown() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>thumbs-down</title><path d="M10 15v4a3 3 0 0 0 3 3l4-9V2H5.72a2 2 0 0 0-2 1.7l-1.38 9a2 2 0 0 0 2 2.3zm7-13h2.67A2.31 2.31 0 0 1 22 4v7a2.31 2.31 0 0 1-2.33 2H17"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// ThumbsUp returns the "thumbs-up" FeatherIcon.
func ThumbsUp() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>thumbs-up</title><path d="M14 9V5a3 3 0 0 0-3-3l-4 9v11h11.28a2 2 0 0 0 2-1.7l1.38-9a2 2 0 0 0-2-2.3zM7 22H4a2 2 0 0 1-2-2v-7a2 2 0 0 1 2-2h3"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// ToggleLeft returns the "toggle-left" FeatherIcon.
func ToggleLeft() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>toggle-left</title><rect x="1" y="5" width="22" height="14" rx="7" ry="7"></rect><circle cx="8" cy="12" r="3"></circle></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// ToggleRight returns the "toggle-right" FeatherIcon.
func ToggleRight() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>toggle-right</title><rect x="1" y="5" width="22" height="14" rx="7" ry="7"></rect><circle cx="16" cy="12" r="3"></circle></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Tool returns the "tool" FeatherIcon.
func Tool() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>tool</title><path d="M14.7 6.3a1 1 0 0 0 0 1.4l1.6 1.6a1 1 0 0 0 1.4 0l3.77-3.77a6 6 0 0 1-7.94 7.94l-6.91 6.91a2.12 2.12 0 0 1-3-3l6.91-6.91a6 6 0 0 1 7.94-7.94l-3.76 3.76z"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Trash returns the "trash" FeatherIcon.
func Trash() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>trash</title><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Trash2 returns the "trash-2" FeatherIcon.
func Trash2() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>trash-2</title><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Trello returns the "trello" FeatherIcon.
func Trello() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>trello</title><rect x="3" y="3" width="18" height="18" rx="2" ry="2"></rect><rect x="7" y="7" width="3" height="9"></rect><rect x="14" y="7" width="3" height="5"></rect></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// TrendingDown returns the "trending-down" FeatherIcon.
func TrendingDown() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>trending-down</title><polyline points="23 18 13.5 8.5 8.5 13.5 1 6"></polyline><polyline points="17 18 23 18 23 12"></polyline></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// TrendingUp returns the "trending-up" FeatherIcon.
func TrendingUp() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>trending-up</title><polyline points="23 6 13.5 15.5 8.5 10.5 1 18"></polyline><polyline points="17 6 23 6 23 12"></polyline></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Triangle returns the "triangle" FeatherIcon.
func Triangle() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>triangle</title><path d="M10.29 3.86L1.82 18a2 2 0 0 0 1.71 3h16.94a2 2 0 0 0 1.71-3L13.71 3.86a2 2 0 0 0-3.42 0z"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Truck returns the "truck" FeatherIcon.
func Truck() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>truck</title><rect x="1" y="3" width="15" height="13"></rect><polygon points="16 8 20 8 23 11 23 16 16 16 16 8"></polygon><circle cx="5.5" cy="18.5" r="2.5"></circle><circle cx="18.5" cy="18.5" r="2.5"></circle></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Tv returns the "tv" FeatherIcon.
func Tv() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>tv</title><rect x="2" y="7" width="20" height="15" rx="2" ry="2"></rect><polyline points="17 2 12 7 7 2"></polyline></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Twitch returns the "twitch" FeatherIcon.
func Twitch() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>twitch</title><path d="M21 2H3v16h5v4l4-4h5l4-4V2zm-10 9V7m5 4V7"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Twitter returns the "twitter" FeatherIcon.
func Twitter() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>twitter</title><path d="M23 3a10.9 10.9 0 0 1-3.14 1.53 4.48 4.48 0 0 0-7.86 3v1A10.66 10.66 0 0 1 3 4s-4 9 5 13a11.64 11.64 0 0 1-7 2c9 5 20 0 20-11.5a4.5 4.5 0 0 0-.08-.83A7.72 7.72 0 0 0 23 3z"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Type returns the "type" FeatherIcon.
func Type() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>type</title><polyline points="4 7 4 4 20 4 20 7"></polyline><line x1="9" y1="20" x2="15" y2="20"></line><line x1="12" y1="4" x2="12" y2="20"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Umbrella returns the "umbrella" FeatherIcon.
func Umbrella() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>umbrella</title><path d="M23 12a11.05 11.05 0 0 0-22 0zm-5 7a3 3 0 0 1-6 0v-7"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Underline returns the "underline" FeatherIcon.
func Underline() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>underline</title><path d="M6 3v7a6 6 0 0 0 6 6 6 6 0 0 0 6-6V3"></path><line x1="4" y1="21" x2="20" y2="21"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Unlock returns the "unlock" FeatherIcon.
func Unlock() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>unlock</title><rect x="3" y="11" width="18" height="11" rx="2" ry="2"></rect><path d="M7 11V7a5 5 0 0 1 9.9-1"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Upload returns the "upload" FeatherIcon.
func Upload() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>upload</title><path d="M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4"></path><polyline points="17 8 12 3 7 8"></polyline><line x1="12" y1="3" x2="12" y2="15"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// UploadCloud returns the "upload-cloud" FeatherIcon.
func UploadCloud() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>upload-cloud</title><polyline points="16 16 12 12 8 16"></polyline><line x1="12" y1="12" x2="12" y2="21"></line><path d="M20.39 18.39A5 5 0 0 0 18 9h-1.26A8 8 0 1 0 3 16.3"></path><polyline points="16 16 12 12 8 16"></polyline></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// User returns the "user" FeatherIcon.
func User() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>user</title><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// UserCheck returns the "user-check" FeatherIcon.
func UserCheck() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>user-check</title><path d="M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="8.5" cy="7" r="4"></circle><polyline points="17 11 19 13 23 9"></polyline></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// UserMinus returns the "user-minus" FeatherIcon.
func UserMinus() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>user-minus</title><path d="M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="8.5" cy="7" r="4"></circle><line x1="23" y1="11" x2="17" y2="11"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// UserPlus returns the "user-plus" FeatherIcon.
func UserPlus() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>user-plus</title><path d="M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="8.5" cy="7" r="4"></circle><line x1="20" y1="8" x2="20" y2="14"></line><line x1="23" y1="11" x2="17" y2="11"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// UserX returns the "user-x" FeatherIcon.
func UserX() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>user-x</title><path d="M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="8.5" cy="7" r="4"></circle><line x1="18" y1="8" x2="23" y2="13"></line><line x1="23" y1="8" x2="18" y2="13"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Users returns the "users" FeatherIcon.
func Users() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>users</title><path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="9" cy="7" r="4"></circle><path d="M23 21v-2a4 4 0 0 0-3-3.87"></path><path d="M16 3.13a4 4 0 0 1 0 7.75"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Video returns the "video" FeatherIcon.
func Video() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>video</title><polygon points="23 7 16 12 23 17 23 7"></polygon><rect x="1" y="5" width="15" height="14" rx="2" ry="2"></rect></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// VideoOff returns the "video-off" FeatherIcon.
func VideoOff() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>video-off</title><path d="M16 16v1a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V7a2 2 0 0 1 2-2h2m5.66 0H14a2 2 0 0 1 2 2v3.34l1 1L23 7v10"></path><line x1="1" y1="1" x2="23" y2="23"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Voicemail returns the "voicemail" FeatherIcon.
func Voicemail() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>voicemail</title><circle cx="5.5" cy="11.5" r="4.5"></circle><circle cx="18.5" cy="11.5" r="4.5"></circle><line x1="5.5" y1="16" x2="18.5" y2="16"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Volume returns the "volume" FeatherIcon.
func Volume() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>volume</title><polygon points="11 5 6 9 2 9 2 15 6 15 11 19 11 5"></polygon></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Volume1 returns the "volume-1" FeatherIcon.
func Volume1() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>volume-1</title><polygon points="11 5 6 9 2 9 2 15 6 15 11 19 11 5"></polygon><path d="M15.54 8.46a5 5 0 0 1 0 7.07"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Volume2 returns the "volume-2" FeatherIcon.
func Volume2() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>volume-2</title><polygon points="11 5 6 9 2 9 2 15 6 15 11 19 11 5"></polygon><path d="M19.07 4.93a10 10 0 0 1 0 14.14M15.54 8.46a5 5 0 0 1 0 7.07"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// VolumeX returns the "volume-x" FeatherIcon.
func VolumeX() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>volume-x</title><polygon points="11 5 6 9 2 9 2 15 6 15 11 19 11 5"></polygon><line x1="23" y1="9" x2="17" y2="15"></line><line x1="17" y1="9" x2="23" y2="15"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Watch returns the "watch" FeatherIcon.
func Watch() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>watch</title><circle cx="12" cy="12" r="7"></circle><polyline points="12 9 12 12 13.5 13.5"></polyline><path d="M16.51 17.35l-.35 3.83a2 2 0 0 1-2 1.82H9.83a2 2 0 0 1-2-1.82l-.35-3.83m.01-10.7l.35-3.83A2 2 0 0 1 9.83 1h4.35a2 2 0 0 1 2 1.82l.35 3.83"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Wifi returns the "wifi" FeatherIcon.
func Wifi() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>wifi</title><path d="M5 12.55a11 11 0 0 1 14.08 0"></path><path d="M1.42 9a16 16 0 0 1 21.16 0"></path><path d="M8.53 16.11a6 6 0 0 1 6.95 0"></path><line x1="12" y1="20" x2="12.01" y2="20"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// WifiOff returns the "wifi-off" FeatherIcon.
func WifiOff() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>wifi-off</title><line x1="1" y1="1" x2="23" y2="23"></line><path d="M16.72 11.06A10.94 10.94 0 0 1 19 12.55"></path><path d="M5 12.55a10.94 10.94 0 0 1 5.17-2.39"></path><path d="M10.71 5.05A16 16 0 0 1 22.58 9"></path><path d="M1.42 9a15.91 15.91 0 0 1 4.7-2.88"></path><path d="M8.53 16.11a6 6 0 0 1 6.95 0"></path><line x1="12" y1="20" x2="12.01" y2="20"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Wind returns the "wind" FeatherIcon.
func Wind() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>wind</title><path d="M9.59 4.59A2 2 0 1 1 11 8H2m10.59 11.41A2 2 0 1 0 14 16H2m15.73-8.27A2.5 2.5 0 1 1 19.5 12H2"></path></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// X returns the "x" FeatherIcon.
func X() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>x</title><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// XCircle returns the "x-circle" FeatherIcon.
func XCircle() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>x-circle</title><circle cx="12" cy="12" r="10"></circle><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// XOctagon returns the "x-octagon" FeatherIcon.
func XOctagon() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>x-octagon</title><polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// XSquare returns the "x-square" FeatherIcon.
func XSquare() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>x-square</title><rect x="3" y="3" width="18" height="18" rx="2" ry="2"></rect><line x1="9" y1="9" x2="15" y2="15"></line><line x1="15" y1="9" x2="9" y2="15"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Youtube returns the "youtube" FeatherIcon.
func Youtube() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>youtube</title><path d="M22.54 6.42a2.78 2.78 0 0 0-1.94-2C18.88 4 12 4 12 4s-6.88 0-8.6.46a2.78 2.78 0 0 0-1.94 2A29 29 0 0 0 1 11.75a29 29 0 0 0 .46 5.33A2.78 2.78 0 0 0 3.4 19c1.72.46 8.6.46 8.6.46s6.88 0 8.6-.46a2.78 2.78 0 0 0 1.94-2 29 29 0 0 0 .46-5.25 29 29 0 0 0-.46-5.33z"></path><polygon points="9.75 15.02 15.5 11.75 9.75 8.48 9.75 15.02"></polygon></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// Zap returns the "zap" FeatherIcon.
func Zap() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>zap</title><polygon points="13 2 3 14 12 14 11 22 21 10 12 10 13 2"></polygon></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// ZapOff returns the "zap-off" FeatherIcon.
func ZapOff() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>zap-off</title><polyline points="12.41 6.75 13 2 10.57 4.92"></polyline><polyline points="18.57 12.91 21 10 15.66 10"></polyline><polyline points="8 8 3 14 12 14 11 22 16 16"></polyline><line x1="1" y1="1" x2="23" y2="23"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// ZoomIn returns the "zoom-in" FeatherIcon.
func ZoomIn() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>zoom-in</title><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line><line x1="11" y1="8" x2="11" y2="14"></line><line x1="8" y1="11" x2="14" y2="11"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}

// ZoomOut returns the "zoom-out" FeatherIcon.
func ZoomOut() *FeatherIcon {
	return &FeatherIcon{
		xml:    `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="%s" id="%s" class="%s"><title>zoom-out</title><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line><line x1="8" y1="11" x2="14" y2="11"></line></svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top;",
	}
}
